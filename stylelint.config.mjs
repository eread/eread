/** @type {import('stylelint').Config} */
export default {
  "extends": "stylelint-config-standard",
  "rules": {
    "at-rule-descriptor-no-unknown": null
  },
};
