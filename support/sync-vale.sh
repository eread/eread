#!/usr/bin/env bash

gum log --time TimeOnly --structured --level info "Syncing Vale packages..."
if [[ -z $CI ]]; then
  gum spin --spinner dot --title "Syncing Vale packages..." --show-error -- vale sync
else
  vale sync
fi
gum log --time TimeOnly --structured --level info "Vale packages synced!"
