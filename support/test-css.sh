#!/usr/bin/env bash

STYLELINT_CMD=(yarn stylelint assets/css/custom.css)
ERROR_CMD=(gum log --time TimeOnly --structured --level error "CSS tests failed!")
YARN_CMD=(yarn install)

gum log --time TimeOnly --structured --level info "Running CSS tests..."
if [[ -z $CI ]]; then
  if [[ ! -d node_modules ]]; then
    gum log --time TimeOnly --structured --level info "node_modules directory not found..."
    gum spin --spinner dot --title "Running yarn install..." --show-error -- "${YARN_CMD[@]}"
    gum log --time TimeOnly --structured --level info "yarn install complete!"
  fi
  if ! gum spin --spinner dot --title "Running CSS tests..." --show-error -- "${STYLELINT_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
else
  if [[ ! -d node_modules ]]; then
    gum log --time TimeOnly --structured --level info "node_modules directory not found..."
    "${YARN_CMD[@]}"
    gum log --time TimeOnly --structured --level info "yarn install complete!"
  fi
  if ! "${STYLELINT_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
fi
gum log --time TimeOnly --structured --level info "CSS tests complete!"
