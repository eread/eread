#!/usr/bin/env bash

gum log --time TimeOnly --structured --level info "Updating Go modules..."
gum spin --spinner dot --title "Cleaning Go modules cache..." --show-error -- go clean -modcache
gum spin --spinner dot --title "Installing latest Go modules..." --show-error -- go get -u
gum spin --spinner dot --title "Removing unsused Go modules entries..." --show-error -- go mod tidy
gum log --time TimeOnly --structured --level info "Go modules updated!"
