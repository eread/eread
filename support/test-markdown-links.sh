#!/usr/bin/env bash

LYCHEE_CMD=(lychee --include-fragments -- *.md)
ERROR_CMD=(gum log --time TimeOnly --structured --level error "Markdown link tests failed!")

gum log --time TimeOnly --structured --level info "Running Markdown link tests..."
if [[ -z $CI ]]; then
  if ! gum spin --spinner dot --title "Running Markdown link tests..." --show-error -- "${LYCHEE_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
else
  if ! "${LYCHEE_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
fi
gum log --time TimeOnly --structured --level info "Markdown link tests complete!"
