#!/usr/bin/env bash
# shellcheck disable=SC2015

FOOTER_TEMPLATE="                                                                      \
   <html>                                                                              \
     <head>                                                                            \
       <style>                                                                         \
         .footer {                                                                     \
           width: 100%;                                                                \
           text-align: center;                                                         \
           font-family: Arial;                                                         \
           font-size: 10px;                                                            \
           color: rgb(15, 23, 42);                                                     \
         }                                                                             \
       </style>                                                                        \
       </head>                                                                         \
       <body>                                                                          \
       <div class=\"footer\">                                                          \
         Page <span class=\"pageNumber\"></span> of <span class=\"totalPages\"></span> \
       </div>                                                                          \
     </body>                                                                           \
   </html>"

if [[ -z $CI ]]; then
  gum spin --spinner dot --title "Creating PDF..." --show-error -- yarn html-export-pdf-cli public/about-me/about-me-single-page.html --preferCSSPageSize --headerTemplate="<html></html>" --footerTemplate="$FOOTER_TEMPLATE" -o content/en/about-evan-read.pdf \
  && gum log --time TimeOnly --structured --level info "Wrote to content/en/about-evan-read.pdf." || (gum log --time TimeOnly --structured --level error "Creating PDF failed!"; exit 1)
else
  yarn html-export-pdf-cli public/about-me/about-me-single-page.html --preferCSSPageSize --headerTemplate="<html></html>" --footerTemplate="$FOOTER_TEMPLATE" -o content/en/about-evan-read.pdf \
  && gum log --time TimeOnly --structured --level info "Wrote to content/en/about-evan-read.pdf." || (gum log --time TimeOnly --structured --level error "Creating PDF failed!"; exit 1)
fi
