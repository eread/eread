#!/usr/bin/env bash

MARKDOWNLINT_CLI2_CMD=(markdownlint-cli2 README.md LISEZ-MOI.md content/**/*.md)
MARKDOWNLINT_CLI2_ERROR_CMD=(gum log --time TimeOnly --structured --level error "markdownlint-cli2 tests failed!")
VALE_CMD=(vale *.md content)
VALE_ERROR_CMD=(gum log --time TimeOnly --structured --level error "Vale tests failed!")

gum log --time TimeOnly --structured --level info "Running Markdown tests..."
if [[ -z $CI ]]; then
  if ! gum spin --spinner dot --title "Running markdownlint-cli2..." --show-error -- "${MARKDOWNLINT_CLI2_CMD[@]}"; then
    "${MARKDOWNLINT_CLI2_ERROR_CMD[@]}"
    exit 1
  fi
  if ! gum spin --spinner dot --title "Running Vale..." --show-error -- "${VALE_CMD[@]}"; then
    "${VALE_ERROR_CMD[@]}"
    exit 1
  fi
else
  if ! "${MARKDOWNLINT_CLI2_CMD[@]}"; then
    "${MARKDOWNLINT_CLI2_ERROR_CMD[@]}"
    exit 1
  fi
  if ! "${VALE_CMD[@]}"; then
    "${VALE_ERROR_CMD[@]}"
    exit 1
  fi
fi
gum log --time TimeOnly --structured --level info "Markdown tests complete!"
