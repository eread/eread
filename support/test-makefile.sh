#!/usr/bin/env bash

CHECKMAKE_CMD=(checkmake Makefile)
ERROR_CMD=(gum log --time TimeOnly --structured --level error "Makefile tests failed!")

if ! command -v checkmake > /dev/null; then
  gum log --time TimeOnly --structured --level error "checkmake not found!"
  gum confirm "Do you want to install Brew dependencies?" && support/install-brew-dependencies.sh || exit 1
fi

gum log --time TimeOnly --structured --level info "Running Makefile tests..."
if [[ -z $CI ]]; then
  if ! gum spin --spinner dot --title "Running Makefile tests..." --show-error -- "${CHECKMAKE_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
else
  if ! "${CHECKMAKE_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
fi
gum log --time TimeOnly --structured --level info "Makefile tests complete!"
