#!/usr/bin/env bash

gum log --time TimeOnly --structured --level info "Building GIF of menu..."
gum spin --spinner dot --title "Building GIF of menu..." --show-error -- vhs cassette.tape
gum log --time TimeOnly --structured --level info "GIF of menu build complete!"
