#!/usr/bin/env bash

RM_PUBLIC_CMD=(rm -rf public)
RM_RESOURCES_CMD=(rm -rf resources)
RM_HUGO_BUILD_LOCK_CMD=(rm -f .hugo_build.lock)
RM_PDF_CMD=(rm -f content/en/about-evan-read.pdf)

echo "${RM_CMD[@]}"

gum log --time TimeOnly --structured --level info "Removing temporary files..."
if [[ -z $CI ]]; then
  gum spin --spinner dot --title "Removing temporary files..." --show-error -- "${RM_PUBLIC_CMD[@]}" && "${RM_RESOURCES_CMD[@]}" && "${RM_HUGO_BUILD_LOCK_CMD[@]}" && "${RM_PDF_CMD[@]}"
else
  "${RM_PUBLIC_CMD[@]}" && "${RM_RESOURCES_CMD[@]}" && "${RM_HUGO_BUILD_LOCK_CMD[@]}" && "${RM_PDF_CMD[@]}"
fi
gum log --time TimeOnly --structured --level info "Temporary files removed!"
