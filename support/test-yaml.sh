#!/usr/bin/env bash

YAMLLINT_CMD=(yamllint .)
ERROR_CMD=(gum log --time TimeOnly --structured --level error "YAML tests failed!")

gum log --time TimeOnly --structured --level info "Running YAML tests..."

if ! command -v yamllint > /dev/null; then
  gum log --time TimeOnly --structured --level error "yamllint not found!"
  exit 1
fi

if [[ -z $CI ]]; then
  if ! gum spin --spinner dot --title "Running YAML tests..." --show-error -- "${YAMLLINT_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
else
  if ! "${YAMLLINT_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
fi

gum log --time TimeOnly --structured --level info "YAML tests complete!"
