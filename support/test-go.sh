#!/usr/bin/env bash

GOLANGCI_LINT_CMD=(golangci-lint run)
ERROR_CMD=(gum log --time TimeOnly --structured --level error "Go tests failed!")

gum log --time TimeOnly --structured --level info "Running Go tests..."
if [[ -z $CI ]]; then
  if ! gum spin --spinner dot --title "Running Go tests..." --show-error -- "${GOLANGCI_LINT_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
else
  if ! "${GOLANGCI_LINT_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
fi
gum log --time TimeOnly --structured --level info "Go tests complete!"
