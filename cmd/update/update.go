package update

import (
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"

	"github.com/Masterminds/semver/v3"
	"github.com/charmbracelet/bubbles/v2/spinner"
	tea "github.com/charmbracelet/bubbletea/v2"
	"github.com/charmbracelet/huh"
	"github.com/charmbracelet/lipgloss/v2"
	"github.com/charmbracelet/lipgloss/v2/table"
	"github.com/charmbracelet/log"
	"github.com/enescakir/emoji"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/eread/eread/cmd"
)

type versionsList struct {
	CheckmakeVersion        string
	ColimaVersion           string
	DockerVersion           string
	FfmpegVersion           string
	GoVersion               string
	GolangciLintVersion     string
	GumVersion              string
	HadolintVersion         string
	HugoVersion             string
	LefthookVersion         string
	LimaVersion             string
	LycheeVersion           string
	MarkdownlintCli2Version string
	NodeVersion             string
	PipxVersion             string
	PuppeteerVersion        string
	ShellcheckVersion       string
	TtydVersion             string
	ValeVersion             string
	VhsVersion              string
	YamllintVersion         string
}

var versionsTable = table.New().
	Border(lipgloss.RoundedBorder()).
	BorderStyle(lipgloss.NewStyle().Foreground(lipgloss.Color("86"))).
	StyleFunc(func(row, col int) lipgloss.Style {
		switch {
		default:
			return lipgloss.NewStyle().Padding(0, 1)
		}
	}).
	Headers("Tool", "Current version", "Latest version")

var currentVersionsList versionsList
var latestVersionsList versionsList
var relativeFilePaths = [...]string{"support/build-docker-image.sh", "mise.toml", ".gitlab-ci.yml"}

type urlList struct {
	CheckmakeURL        string
	ColimaURL           string
	DockerURL           string
	FfmpegURL           string
	GoURL               string
	GolangciLintURL     string
	GumURL              string
	HadolintURL         string
	HugoURL             string
	LefthookURL         string
	LimaURL             string
	LycheeURL           string
	MarkdownlintCli2URL string
	NodeURL             string
	PipxURL             string
	PuppeteerURL        string
	ShellcheckURL       string
	TtydURL             string
	ValeURL             string
	VhsURL              string
	YamllintURL         string
}

var toolsURLList = urlList{
	CheckmakeURL:        "https://github.com/mrtazz/checkmake",
	ColimaURL:           "https://github.com/abiosoft/colima",
	DockerURL:           "https://github.com/docker/cli",
	FfmpegURL:           "https://git.ffmpeg.org/ffmpeg",
	GoURL:               "https://github.com/golang/go",
	GolangciLintURL:     "https://github.com/golangci/golangci-lint",
	GumURL:              "https://github.com/charmbracelet/gum",
	HadolintURL:         "https://github.com/hadolint/hadolint",
	HugoURL:             "https://github.com/gohugoio/hugo",
	LefthookURL:         "https://github.com/evilmartians/lefthook",
	LimaURL:             "https://github.com/lima-vm/lima",
	LycheeURL:           "https://github.com/lycheeverse/lychee",
	MarkdownlintCli2URL: "https://github.com/DavidAnson/markdownlint-cli2",
	NodeURL:             "https://github.com/nodejs/node",
	PipxURL:             "https://github.com/pypa/pipx",
	PuppeteerURL:        "https://github.com/puppeteer/puppeteer",
	ShellcheckURL:       "https://github.com/koalaman/shellcheck",
	TtydURL:             "https://github.com/tsl0922/ttyd",
	ValeURL:             "https://github.com/errata-ai/vale",
	VhsURL:              "https://github.com/charmbracelet/vhs",
	YamllintURL:         "https://github.com/adrienverge/yamllint",
}

type model struct {
	Spinner spinner.Model
	Message string
}

var programSpinner = model{
	Spinner: spinner.New(
		spinner.WithSpinner(spinner.Dot),
		spinner.WithStyle(lipgloss.NewStyle().Foreground(lipgloss.Color("212")))),
	Message: "Checking for latest tooling versions...",
}

var logger = log.NewWithOptions(os.Stderr, log.Options{
	ReportTimestamp: true,
	TimeFormat:      time.TimeOnly,
})

var Yes bool

var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "Check for project dependency updates",
	Long:  "Check for available updates to a variety of project dependencies and update configuration files.",
	Run: func(cmd *cobra.Command, args []string) {
		getCurrentVersions()

		if !Yes {
			confirmation := true
			confirmUpdates := huh.NewConfirm().
				Title(emoji.Parse("Check for updates? :wrench:")).
				Affirmative("Yes!").
				Negative("No").
				Value(&confirmation)
			err := confirmUpdates.Run()
			if err != nil {
				logger.Fatalf("%s", err)
			}

			if !confirmation {
				logger.Warn("Update check cancelled!")
				os.Exit(1)
			} else {
				logger.Info("Checking for updates!")
			}
		} else {
			logger.Info("Checking for updates!")
		}

		logger.Info("Checking for latest tooling versions...")
		program := tea.NewProgram(programSpinner)

		if _, err := program.Run(); err != nil {
			logger.Fatalf("%s", err)
		} else {
			fmt.Println(versionsTable)
			logger.Info("Latest tooling versions checked!")
			logger.Infof("Wrote to %s, %s, and %s!", relativeFilePaths[0], relativeFilePaths[1], relativeFilePaths[2])
		}
	},
}

func updatePipelineConfigurationFile() {
	pipelineConfigurationFile, err := os.ReadFile(relativeFilePaths[2])
	if err != nil {
		logger.Fatalf("%s", err)
	}
	pipelineConfigurationFileLines := strings.Split(string(pipelineConfigurationFile), "\n")
	for i, pipelineConfigurationFileLine := range pipelineConfigurationFileLines {
		if strings.HasPrefix(pipelineConfigurationFileLine, "  CHECKMAKE_VERSION:") {
			pipelineConfigurationFileLines[i] = "  CHECKMAKE_VERSION: " + latestVersionsList.CheckmakeVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  GO_VERSION:") {
			pipelineConfigurationFileLines[i] = "  GO_VERSION: " + latestVersionsList.GoVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  GOLANGCI_LINT_VERSION:") {
			pipelineConfigurationFileLines[i] = "  GOLANGCI_LINT_VERSION: " + latestVersionsList.GolangciLintVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  GUM_VERSION:") {
			pipelineConfigurationFileLines[i] = "  GUM_VERSION: " + latestVersionsList.GumVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  HADOLINT_VERSION:") {
			pipelineConfigurationFileLines[i] = "  HADOLINT_VERSION: " + latestVersionsList.HadolintVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  HUGO_VERSION:") {
			pipelineConfigurationFileLines[i] = "  HUGO_VERSION: " + latestVersionsList.HugoVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  LYCHEE_VERSION:") {
			pipelineConfigurationFileLines[i] = "  LYCHEE_VERSION: " + latestVersionsList.LycheeVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  MARKDOWNLINT_CLI2_VERSION:") {
			pipelineConfigurationFileLines[i] = "  MARKDOWNLINT_CLI2_VERSION: " + latestVersionsList.MarkdownlintCli2Version
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  NODE_VERSION:") {
			pipelineConfigurationFileLines[i] = "  NODE_VERSION: " + latestVersionsList.NodeVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  PUPPETEER_IMAGE:") {
			pipelineConfigurationFileLines[i] = "  PUPPETEER_IMAGE: ghcr.io/puppeteer/puppeteer:" + latestVersionsList.PuppeteerVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  SHELLCHECK_VERSION:") {
			pipelineConfigurationFileLines[i] = "  SHELLCHECK_VERSION: " + latestVersionsList.ShellcheckVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  TTYD_VERSION:") {
			pipelineConfigurationFileLines[i] = "  TTYD_VERSION: " + latestVersionsList.TtydVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  VALE_VERSION:") {
			pipelineConfigurationFileLines[i] = "  VALE_VERSION: " + latestVersionsList.ValeVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  VHS_VERSION:") {
			pipelineConfigurationFileLines[i] = "  VHS_VERSION: " + latestVersionsList.VhsVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  YAMLLINT_VERSION:") {
			pipelineConfigurationFileLines[i] = "  YAMLLINT_VERSION: " + latestVersionsList.YamllintVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  DOCKER_BUILD_IMAGE:") {
			pipelineConfigurationFileLines[i] = "  DOCKER_BUILD_IMAGE: docker:" + latestVersionsList.DockerVersion
		}
		if strings.HasPrefix(pipelineConfigurationFileLine, "  DOCKER_IN_DOCKER_SERVICE:") {
			pipelineConfigurationFileLines[i] = "  DOCKER_IN_DOCKER_SERVICE: docker:" + latestVersionsList.DockerVersion + "-dind"
		}
	}
	dataToWrite := strings.Join(pipelineConfigurationFileLines, "\n")
	err = os.WriteFile(relativeFilePaths[2], []byte(dataToWrite), 0644)
	if err != nil {
		logger.Fatalf("%s", err)
	}
}

func updateMiseConfigurationFile() {
	miseConfigurationFile, err := os.ReadFile(relativeFilePaths[1])
	if err != nil {
		logger.Fatalf("%s", err)
	}
	miseConfigurationFileLines := strings.Split(string(miseConfigurationFile), "\n")
	for i, miseConfigurationFileLine := range miseConfigurationFileLines {
		if strings.HasPrefix(miseConfigurationFileLine, "checkmake =") {
			miseConfigurationFileLines[i] = "checkmake = '" + latestVersionsList.CheckmakeVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "colima =") {
			miseConfigurationFileLines[i] = "colima = '" + latestVersionsList.ColimaVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "ffmpeg =") {
			miseConfigurationFileLines[i] = "ffmpeg = '" + latestVersionsList.FfmpegVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "gum =") {
			miseConfigurationFileLines[i] = "gum = '" + latestVersionsList.GumVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "golang =") {
			miseConfigurationFileLines[i] = "golang = '" + latestVersionsList.GoVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "golangci-lint =") {
			miseConfigurationFileLines[i] = "golangci-lint = '" + latestVersionsList.GolangciLintVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "hadolint =") {
			miseConfigurationFileLines[i] = "hadolint = '" + latestVersionsList.HadolintVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "hugo =") {
			miseConfigurationFileLines[i] = "hugo = '" + latestVersionsList.HugoVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "lefthook =") {
			miseConfigurationFileLines[i] = "lefthook = '" + latestVersionsList.LefthookVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "lima =") {
			miseConfigurationFileLines[i] = "lima = '" + latestVersionsList.LimaVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "lychee =") {
			miseConfigurationFileLines[i] = "lychee = '" + latestVersionsList.LycheeVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "markdownlint-cli2 =") {
			miseConfigurationFileLines[i] = "markdownlint-cli2 = '" + latestVersionsList.MarkdownlintCli2Version + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "nodejs =") {
			miseConfigurationFileLines[i] = "nodejs = '" + latestVersionsList.NodeVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "pipx =") {
			miseConfigurationFileLines[i] = "pipx = '" + latestVersionsList.PipxVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "shellcheck =") {
			miseConfigurationFileLines[i] = "shellcheck = '" + latestVersionsList.ShellcheckVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "ttyd =") {
			miseConfigurationFileLines[i] = "ttyd = '" + latestVersionsList.TtydVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "vale =") {
			miseConfigurationFileLines[i] = "vale = '" + latestVersionsList.ValeVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "vhs =") {
			miseConfigurationFileLines[i] = "vhs = '" + latestVersionsList.VhsVersion + "'"
		}
		if strings.HasPrefix(miseConfigurationFileLine, "yamllint =") {
			miseConfigurationFileLines[i] = "yamllint = '" + latestVersionsList.YamllintVersion + "'"
		}
	}
	dataToWrite := strings.Join(miseConfigurationFileLines, "\n")
	err = os.WriteFile(relativeFilePaths[1], []byte(dataToWrite), 0644)
	if err != nil {
		logger.Fatalf("%s", err)
	}
}

func updateShellScriptFile() {
	shellScriptFile, err := os.ReadFile(relativeFilePaths[0])
	if err != nil {
		logger.Fatalf("%s", err)
	}
	shellScriptFileLines := strings.Split(string(shellScriptFile), "\n")
	for i, shellScriptFileLine := range shellScriptFileLines {
		if strings.HasPrefix(shellScriptFileLine, "CHECKMAKE_VERSION=") {
			shellScriptFileLines[i] = "CHECKMAKE_VERSION=" + latestVersionsList.CheckmakeVersion
		}
		if strings.HasPrefix(shellScriptFileLine, "GO_VERSION=") {
			shellScriptFileLines[i] = "GO_VERSION=" + latestVersionsList.GoVersion
		}
		if strings.HasPrefix(shellScriptFileLine, "GOLANGCI_LINT_VERSION=") {
			shellScriptFileLines[i] = "GOLANGCI_LINT_VERSION=" + latestVersionsList.GolangciLintVersion
		}
		if strings.HasPrefix(shellScriptFileLine, "GUM_VERSION=") {
			shellScriptFileLines[i] = "GUM_VERSION=" + latestVersionsList.GumVersion
		}
		if strings.HasPrefix(shellScriptFileLine, "HADOLINT_VERSION=") {
			shellScriptFileLines[i] = "HADOLINT_VERSION=" + latestVersionsList.HadolintVersion
		}
		if strings.HasPrefix(shellScriptFileLine, "HUGO_VERSION=") {
			shellScriptFileLines[i] = "HUGO_VERSION=" + latestVersionsList.HugoVersion
		}
		if strings.HasPrefix(shellScriptFileLine, "LYCHEE_VERSION=") {
			shellScriptFileLines[i] = "LYCHEE_VERSION=" + latestVersionsList.LycheeVersion
		}
		if strings.HasPrefix(shellScriptFileLine, "MARKDOWNLINT_CLI2_VERSION=") {
			shellScriptFileLines[i] = "MARKDOWNLINT_CLI2_VERSION=" + latestVersionsList.MarkdownlintCli2Version
		}
		if strings.HasPrefix(shellScriptFileLine, "NODE_VERSION=") {
			shellScriptFileLines[i] = "NODE_VERSION=" + latestVersionsList.NodeVersion
		}
		if strings.HasPrefix(shellScriptFileLine, "SHELLCHECK_VERSION=") {
			shellScriptFileLines[i] = "SHELLCHECK_VERSION=" + latestVersionsList.ShellcheckVersion
		}
		if strings.HasPrefix(shellScriptFileLine, "VALE_VERSION=") {
			shellScriptFileLines[i] = "VALE_VERSION=" + latestVersionsList.ValeVersion
		}
		if strings.HasPrefix(shellScriptFileLine, "YAMLLINT_VERSION=") {
			shellScriptFileLines[i] = "YAMLLINT_VERSION=" + latestVersionsList.YamllintVersion
		}
	}
	dataToWrite := strings.Join(shellScriptFileLines, "\n")
	err = os.WriteFile(relativeFilePaths[0], []byte(dataToWrite), 0644)
	if err != nil {
		logger.Fatalf("%s", err)
	}
}

func updateFiles() tea.Msg {
	updateShellScriptFile()
	updatePipelineConfigurationFile()
	updateMiseConfigurationFile()

	return "tea message"
}

func getVersionsTable() tea.Msg {
	toolURLStyle := lipgloss.NewStyle().Underline(true).Foreground(lipgloss.Color("63"))
	tableData := [][]string{
		{"checkmake " + toolURLStyle.Render(toolsURLList.CheckmakeURL), currentVersionsList.CheckmakeVersion, latestVersionsList.CheckmakeVersion},
		{"Colima " + toolURLStyle.Render(toolsURLList.ColimaURL), currentVersionsList.ColimaVersion, latestVersionsList.ColimaVersion},
		{"Docker " + toolURLStyle.Render(toolsURLList.DockerURL), currentVersionsList.DockerVersion, latestVersionsList.DockerVersion},
		{"FFmpeg " + toolURLStyle.Render(toolsURLList.FfmpegURL), currentVersionsList.FfmpegVersion, latestVersionsList.FfmpegVersion},
		{"Go " + toolURLStyle.Render(toolsURLList.GoURL), currentVersionsList.GoVersion, latestVersionsList.GoVersion},
		{"golangci-lint " + toolURLStyle.Render(toolsURLList.GolangciLintURL), currentVersionsList.GolangciLintVersion, latestVersionsList.GolangciLintVersion},
		{"Gum " + toolURLStyle.Render(toolsURLList.GumURL), currentVersionsList.GumVersion, latestVersionsList.GumVersion},
		{"hadolint " + toolURLStyle.Render(toolsURLList.HadolintURL), currentVersionsList.HadolintVersion, latestVersionsList.HadolintVersion},
		{"Hugo " + toolURLStyle.Render(toolsURLList.HugoURL), currentVersionsList.HugoVersion, latestVersionsList.HugoVersion},
		{"Lefthook " + toolURLStyle.Render(toolsURLList.LefthookURL), currentVersionsList.LefthookVersion, latestVersionsList.LefthookVersion},
		{"Lima " + toolURLStyle.Render(toolsURLList.LimaURL), currentVersionsList.LimaVersion, latestVersionsList.LimaVersion},
		{"Lychee " + toolURLStyle.Render(toolsURLList.LycheeURL), currentVersionsList.LycheeVersion, latestVersionsList.LycheeVersion},
		{"markdownlint-cli2 " + toolURLStyle.Render(toolsURLList.MarkdownlintCli2URL), currentVersionsList.MarkdownlintCli2Version, latestVersionsList.MarkdownlintCli2Version},
		{"Node.js " + toolURLStyle.Render(toolsURLList.NodeURL), currentVersionsList.NodeVersion, latestVersionsList.NodeVersion},
		{"pipx " + toolURLStyle.Render(toolsURLList.PipxURL), currentVersionsList.PipxVersion, latestVersionsList.PipxVersion},
		{"Puppeteer " + toolURLStyle.Render(toolsURLList.PuppeteerURL), currentVersionsList.PuppeteerVersion, latestVersionsList.PuppeteerVersion},
		{"ShellCheck " + toolURLStyle.Render(toolsURLList.ShellcheckURL), currentVersionsList.ShellcheckVersion, latestVersionsList.ShellcheckVersion},
		{"ttyd " + toolURLStyle.Render(toolsURLList.TtydURL), currentVersionsList.TtydVersion, latestVersionsList.TtydVersion},
		{"Vale " + toolURLStyle.Render(toolsURLList.ValeURL), currentVersionsList.ValeVersion, latestVersionsList.ValeVersion},
		{"VHS " + toolURLStyle.Render(toolsURLList.VhsURL), currentVersionsList.VhsVersion, latestVersionsList.VhsVersion},
		{"yamllint " + toolURLStyle.Render(toolsURLList.YamllintURL), currentVersionsList.YamllintVersion, latestVersionsList.YamllintVersion},
	}

	for i := range tableData {
		if strings.HasPrefix(tableData[i][0], "Lychee") {
			continue
		}
		currentSemVerVersion, err := semver.NewVersion(tableData[i][1])
		if err != nil {
			logger.Fatalf("%s", err)
		}
		latestSemVerVersion, err := semver.NewVersion(tableData[i][2])
		if err != nil {
			logger.Fatalf("%s", err)
		}
		if currentSemVerVersion.LessThan(latestSemVerVersion) {
			tableData[i][2] = lipgloss.NewStyle().Foreground(lipgloss.Color("63")).Render(tableData[i][2] + " (new)")
		}
	}

	versionsTable.Rows(tableData...)
	return versionsTable
}

func getCurrentVersions() {
	viper.SetConfigFile(".gitlab-ci.yml")
	err := viper.ReadInConfig()
	if err != nil {
		logger.Fatalf("%s", err)
	}
	currentVersionsList.DockerVersion = strings.Replace(viper.GetString("variables.DOCKER_BUILD_IMAGE"), "docker:", "", 1)
	currentVersionsList.GoVersion = viper.GetString("variables.GO_VERSION")
	currentVersionsList.GolangciLintVersion = viper.GetString("variables.GOLANGCI_LINT_VERSION")
	currentVersionsList.GumVersion = viper.GetString("variables.GUM_VERSION")
	currentVersionsList.HadolintVersion = viper.GetString("variables.HADOLINT_VERSION")
	currentVersionsList.HugoVersion = viper.GetString("variables.HUGO_VERSION")
	currentVersionsList.MarkdownlintCli2Version = viper.GetString("variables.MARKDOWNLINT_CLI2_VERSION")
	currentVersionsList.NodeVersion = viper.GetString("variables.NODE_VERSION")
	currentVersionsList.PuppeteerVersion = strings.Replace(viper.GetString("variables.PUPPETEER_IMAGE"), "ghcr.io/puppeteer/puppeteer:", "", 1)
	currentVersionsList.ShellcheckVersion = viper.GetString("variables.SHELLCHECK_VERSION")
	currentVersionsList.TtydVersion = viper.GetString("variables.TTYD_VERSION")
	currentVersionsList.ValeVersion = viper.GetString("variables.VALE_VERSION")
	currentVersionsList.VhsVersion = viper.GetString("variables.VHS_VERSION")
	currentVersionsList.YamllintVersion = viper.GetString("variables.YAMLLINT_VERSION")

	viper.SetConfigFile("mise.toml")
	err = viper.ReadInConfig()
	if err != nil {
		logger.Fatalf("%s", err)
	}
	currentVersionsList.CheckmakeVersion = viper.GetString("tools.checkmake")
	currentVersionsList.ColimaVersion = viper.GetString("tools.colima")
	currentVersionsList.FfmpegVersion = viper.GetString("tools.ffmpeg")
	currentVersionsList.LefthookVersion = viper.GetString("tools.lefthook")
	currentVersionsList.LimaVersion = viper.GetString("tools.lima")
	currentVersionsList.LycheeVersion = viper.GetString("tools.lychee")
	currentVersionsList.PipxVersion = viper.GetString("tools.pipx")
}

func containsNoUnwantedStrings(versionString string) bool {
	if strings.Contains(versionString, "3.0-beta") || strings.Contains(versionString, "3.1-beta") ||
		strings.Contains(versionString, "release") || strings.Contains(versionString, "rc") ||
		strings.Contains(versionString, "weekly") || strings.Contains(versionString, "dev") {
		return false
	} else {
		return true
	}
}

func getLatestVersion(toolURL string) string {
	versionList, _ := exec.Command("git", "ls-remote", "--tags", "--refs", "--sort=version:refname", toolURL).Output()
	versionListUnfiltered := strings.Split(string(versionList), "\n")
	latestVersion := ""
	for i := len(versionListUnfiltered) - 2; i >= 0; i-- {
		if toolURL == toolsURLList.FfmpegURL && strings.Contains(versionListUnfiltered[i], "refs/tags/n") && containsNoUnwantedStrings(versionListUnfiltered[i]) {
			re := regexp.MustCompile("refs/tags/n(.+)")
			latestVersion = re.FindStringSubmatch(versionListUnfiltered[i])[1]
			break
		} else if toolURL == toolsURLList.LycheeURL && strings.Contains(versionListUnfiltered[i], "lychee") {
			re := regexp.MustCompile("refs/tags/lychee-v(.+)")
			latestVersion = re.FindStringSubmatch(versionListUnfiltered[i])[1]
			break
		} else if toolURL == toolsURLList.PuppeteerURL && strings.Contains(versionListUnfiltered[i], "puppeteer") {
			re := regexp.MustCompile("refs/tags/puppeteer-v(.+)")
			latestVersion = re.FindStringSubmatch(versionListUnfiltered[i])[1]
			break
		} else if toolURL == toolsURLList.GoURL && containsNoUnwantedStrings(versionListUnfiltered[i]) {
			re := regexp.MustCompile("refs/tags/go(.+)")
			latestVersion = re.FindStringSubmatch(versionListUnfiltered[i])[1]
			break
		} else if (toolURL != toolsURLList.FfmpegURL && toolURL != toolsURLList.LycheeURL && toolURL != toolsURLList.PuppeteerURL) && containsNoUnwantedStrings(versionListUnfiltered[i]) {
			re := regexp.MustCompile("refs/tags/v?(.+)")
			latestVersion = re.FindStringSubmatch(versionListUnfiltered[i])[1]
			break
		}
	}
	return latestVersion
}

func getLatestVersions() tea.Msg {
	latestVersionsList = versionsList{
		ColimaVersion:           getLatestVersion(toolsURLList.ColimaURL),
		CheckmakeVersion:        getLatestVersion(toolsURLList.CheckmakeURL),
		DockerVersion:           getLatestVersion(toolsURLList.DockerURL),
		FfmpegVersion:           getLatestVersion(toolsURLList.FfmpegURL),
		GoVersion:               getLatestVersion(toolsURLList.GoURL),
		GolangciLintVersion:     getLatestVersion(toolsURLList.GolangciLintURL),
		GumVersion:              getLatestVersion(toolsURLList.GumURL),
		HadolintVersion:         getLatestVersion(toolsURLList.HadolintURL),
		HugoVersion:             getLatestVersion(toolsURLList.HugoURL),
		LefthookVersion:         getLatestVersion(toolsURLList.LefthookURL),
		LimaVersion:             getLatestVersion(toolsURLList.LimaURL),
		LycheeVersion:           getLatestVersion(toolsURLList.LycheeURL),
		MarkdownlintCli2Version: getLatestVersion(toolsURLList.MarkdownlintCli2URL),
		NodeVersion:             getLatestVersion(toolsURLList.NodeURL),
		PipxVersion:             getLatestVersion(toolsURLList.PipxURL),
		PuppeteerVersion:        getLatestVersion(toolsURLList.PuppeteerURL),
		ShellcheckVersion:       getLatestVersion(toolsURLList.ShellcheckURL),
		TtydVersion:             getLatestVersion(toolsURLList.TtydURL),
		ValeVersion:             getLatestVersion(toolsURLList.ValeURL),
		VhsVersion:              getLatestVersion(toolsURLList.VhsURL),
		YamllintVersion:         getLatestVersion(toolsURLList.YamllintURL),
	}
	return latestVersionsList
}

func (m model) Init() tea.Cmd {
	return tea.Sequence(
		m.Spinner.Tick,
		getLatestVersions,
		getVersionsTable,
		updateFiles,
	)
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case versionsList:
		var cmd tea.Cmd
		m.Message = string("Printing versions table...")
		m.Spinner, cmd = m.Spinner.Update(msg)
		return m, cmd
	case table.Table:
		var cmd tea.Cmd
		m.Message = string("Writing to files...")
		m.Spinner, cmd = m.Spinner.Update(msg)
		return m, cmd
	case string:
		return m, tea.Quit
	default:
		var cmd tea.Cmd
		m.Spinner, cmd = m.Spinner.Update(msg)
		return m, cmd
	}
}

func (m model) View() string {
	return fmt.Sprintf("%s %s", m.Spinner.View(), m.Message)
}

func init() {
	cmd.RootCmd.AddCommand(updateCmd)
	updateCmd.Flags().BoolVarP(&Yes, "yes", "y", false, "Skip confirmation before checking for updates")

}
