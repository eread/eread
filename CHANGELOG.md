<!-- vale off -->

# Changelog for README of Evan Read project

## [1.16.0](https://gitlab.com/eread/eread/compare/v1.15.0...v1.16.0) (2025-01-28)

### 🖋️ Content changes 🖋️

* add link to docs-gitlab-com project ([2149ede](https://gitlab.com/eread/eread/commit/2149ede3cd35c5f255edeb2eda0ba1697962b015))
* adjust page margins for PDF ([9385919](https://gitlab.com/eread/eread/commit/93859192e4148c5f360c5dcababaac45034e67c4))
* update link to moved project ([884d19d](https://gitlab.com/eread/eread/commit/884d19d034ec9f983a4d41b29e0160ef88017534))

### 🧹 Project chores 🧹

* remove unneeded spelling exception ([aa0edef](https://gitlab.com/eread/eread/commit/aa0edef12507d7a9f385db7df2f712cb61c848b1))
* update project dependencies ([1d9e1f3](https://gitlab.com/eread/eread/commit/1d9e1f3d5104ef2ded69f8cc813c680bf909a151))
* update project dependencies ([f1b07d9](https://gitlab.com/eread/eread/commit/f1b07d91527993d7bae38e87ef9d2df5b6e9db9d))
* update some project dependencies ([56705ee](https://gitlab.com/eread/eread/commit/56705eec111a88a5e097254265e681a32679138d))

### 🧱 Project features 🧱

* manage Colima dependency by using mise rather than brew ([9d8471f](https://gitlab.com/eread/eread/commit/9d8471ff99baef069d8184e67164f04d6635dcf1))
* manage gum dependency by using mise rather than brew ([0ee865d](https://gitlab.com/eread/eread/commit/0ee865d6822ee9ca5148efa82e3549a7377bf2b1))
* manage hadolint dependency by using mise rather than brew ([1f0a46c](https://gitlab.com/eread/eread/commit/1f0a46c44f4c45e41d669442c464e457f52df067))
* manage pipx dependency by using mise rather than brew ([43a09eb](https://gitlab.com/eread/eread/commit/43a09eb96fe3bd3096de20c62b069aaba230de4d))
* manage ttyd dependency by using mise ([6ba8e8d](https://gitlab.com/eread/eread/commit/6ba8e8d5cea36cc1ee9249aba77f4ace46446fb3))
* manage VHS dependency by using mise rather than brew ([c8c146b](https://gitlab.com/eread/eread/commit/c8c146b16802e3a3eb0a5e2f2a748b8b3e9a9c4f))

## [1.15.0](https://gitlab.com/eread/eread/compare/v1.14.0...v1.15.0) (2025-01-13)

### 🖋️ Content changes 🖋️

* note French as the language I'm most interested in ([56a4366](https://gitlab.com/eread/eread/commit/56a43664a7c77248576dc184faf6b533110b6dac))

### 🧹 Project chores 🧹

* update chromium dependency ([e51f82e](https://gitlab.com/eread/eread/commit/e51f82e6d1e05e05ced9164500eddcfc21952cad))
* update curl dependency in Docker images ([170ff46](https://gitlab.com/eread/eread/commit/170ff4692696c831767d280e43e542404ed003f9))
* update curl in Docker image ([f296cc9](https://gitlab.com/eread/eread/commit/f296cc9ba6163797531a0ab27953fd0d1fb121f8))
* update Docker dependency ([559040e](https://gitlab.com/eread/eread/commit/559040e69c4c10837f0a44455f7baa26f214a0c8))
* update eslint and lefthook ([9b871c7](https://gitlab.com/eread/eread/commit/9b871c7da8ce9448452913e8fed4c632fd9c5259))
* update git package in Docker image ([106ddfd](https://gitlab.com/eread/eread/commit/106ddfd0abf1fff78ec26cc68f7a5707cec70c01))
* update Hugo, lefthook, and Vale versions in project ([4e57f9f](https://gitlab.com/eread/eread/commit/4e57f9ff1b1850bedb08ca46c3d05d899e54cb3f))
* update lefthook and other project dependencies ([f96d46b](https://gitlab.com/eread/eread/commit/f96d46b4cf619db9803a5201399a6523827dd8fa))
* update lefthook dependency ([d94b1b6](https://gitlab.com/eread/eread/commit/d94b1b679fdaab4945a0ae118fe82d41b022fb0d))
* update Lefthook dependency ([ad1f696](https://gitlab.com/eread/eread/commit/ad1f696435ff4ce06243d681e43b6c41034ab3c9))
* update other project dependencies ([8a6e255](https://gitlab.com/eread/eread/commit/8a6e255d6c800e8e379346366b09fa209c383cae))
* update project dependencies ([d3e9171](https://gitlab.com/eread/eread/commit/d3e91717306264ee8113e809bc91a315267d9342))
* update project dependencies ([8ea0286](https://gitlab.com/eread/eread/commit/8ea02863e8d285d70f1bea5adcf94483c13b2652))
* update project dependencies ([e4f38c5](https://gitlab.com/eread/eread/commit/e4f38c58490d7ca0ed344e4db4ef95895f9b710f))
* update project dependencies ([c7315fd](https://gitlab.com/eread/eread/commit/c7315fd6ca7128f653297a1a9bae262fff5a02f8))
* update project dependencies ([ba39764](https://gitlab.com/eread/eread/commit/ba39764441dd0749f2d5f6ddf07de4d30e7280c7))
* update project dependencies ([df1eba4](https://gitlab.com/eread/eread/commit/df1eba468e29b166839f2396ff7e404734d495aa))
* update project dependencies ([3f40333](https://gitlab.com/eread/eread/commit/3f403332faebc9306142058747544da382dcd411))
* update project dependencies ([0a3496c](https://gitlab.com/eread/eread/commit/0a3496c65f72abd786eee60e2656f425d7fb59ef))
* update project dependencies ([df3e8d2](https://gitlab.com/eread/eread/commit/df3e8d28630876935da0dac6705417e445dd23fe))
* update project dependencies ([cfea3b3](https://gitlab.com/eread/eread/commit/cfea3b327de2cd32455ff8f1b403f693ae097d75))
* update project dependencies ([5437b4b](https://gitlab.com/eread/eread/commit/5437b4b15a31cad2c8dd2d14e2eb8964cb27b730))
* update project dependencies ([7e2aa60](https://gitlab.com/eread/eread/commit/7e2aa6057e14f26489db7262f7c26b2ead24846a))
* update project dependencies ([64a031e](https://gitlab.com/eread/eread/commit/64a031ee9e12ef10b268e358d80e869774a495a9))
* update project dependencies ([e2469d2](https://gitlab.com/eread/eread/commit/e2469d2ce104f61064864a1585dde8b234bee311))
* update project dependencies ([6493c9c](https://gitlab.com/eread/eread/commit/6493c9c98ee3e7a9bb7d8b1ae4a78833c49d3666))
* update project dependencies ([000e390](https://gitlab.com/eread/eread/commit/000e390e1766e568039122b6ce057c7d42d559cf))
* update project dependencies ([d4fcc62](https://gitlab.com/eread/eread/commit/d4fcc62b6b4081a1e8ecc7f2c05381e4b63bc9b2))
* update project dependencies including Go modules ([bb74bcc](https://gitlab.com/eread/eread/commit/bb74bccb03b527d6300b37e005393def99c8cb42))
* update some dependencies ([d1942e7](https://gitlab.com/eread/eread/commit/d1942e7790eb0665d54d709786a0a4e7df75cd00))
* update some project dependencies ([f59e059](https://gitlab.com/eread/eread/commit/f59e059f85ecb078a1d759fff03af582e8e55c2d))
* update Vale and Gum versions from Alpine ([091fdc1](https://gitlab.com/eread/eread/commit/091fdc1ee99561d047cc1be0728080f827ff42db))
* update version dependencies ([0b8c0cf](https://gitlab.com/eread/eread/commit/0b8c0cf1326258956e2aece8a999a694a155911f))
* update versions in Dockerfile ([0411210](https://gitlab.com/eread/eread/commit/04112103d3f737d84f613c040c4990c415a36ade))
* update xz-utils package in Docker image ([c798c01](https://gitlab.com/eread/eread/commit/c798c0191b99f26636d16bc489b392e78adbf82a))
* update yamllint in Docker image ([694c000](https://gitlab.com/eread/eread/commit/694c0002f8c1d374bd4321d006ffe6322f211d06))
* update yarn dependencies and resolve peer dependency errors ([83d9a67](https://gitlab.com/eread/eread/commit/83d9a6749ca2650ebe4d54a3d5b2e50cdb42e8f4))
* use regular Docker image for Markdown testing again ([091c12d](https://gitlab.com/eread/eread/commit/091c12d1ca2c549249b42b6e31ce8822dcab14ab))

### 📔 Documentation updates 📔

* add 'autoupdate' to list of OMZ plugins ([7e81153](https://gitlab.com/eread/eread/commit/7e811534a06b79e794adbcc2fb407af11a5bea07))
* add GDK configuration information ([99ef77a](https://gitlab.com/eread/eread/commit/99ef77ae072530c2d4d2d543d036605ec34216bd))
* add information about Slufigy Sublime Text package ([1565751](https://gitlab.com/eread/eread/commit/156575115e7a0434eac165ca423f636145906ef0))
* add information about SublimeLinter configuration ([5d8e96e](https://gitlab.com/eread/eread/commit/5d8e96e075e4bbf9b8ac22ce2e9be96b88d497b0))
* add information about SublimeLinter-eslint configuration ([b3a7d8b](https://gitlab.com/eread/eread/commit/b3a7d8bc3960fca4f12d475a45cd18c4bea71936))
* add information on Gomod and VHS Sublime Text packages ([546aff6](https://gitlab.com/eread/eread/commit/546aff6820b6d8ba75246eebca6254f6a4254a20))
* add link to GitLab Unfiltered content ([7ffbdc0](https://gitlab.com/eread/eread/commit/7ffbdc01e126561550c8f49a9a8097fda87f69f8))
* add more GDK configuration settings ([d3e3d3c](https://gitlab.com/eread/eread/commit/d3e3d3ccf6b8a2d253cd5aabb31521ecf0b9a241))
* add Origami to list of Sublime Text packages ([eeaeedf](https://gitlab.com/eread/eread/commit/eeaeedf69203ef636dee690c877aa8bc4c344477))
* add SublimeLinter-rubocop configuration information ([2ff9dad](https://gitlab.com/eread/eread/commit/2ff9dad9dd09e4ef789457f010fade17ad2b767d))
* add zsh-completions to list of Zsh plugins ([4f4d64e](https://gitlab.com/eread/eread/commit/4f4d64ed3a9c51125c191aabfb907db6add92fff))
* formatting tweak ([72441b3](https://gitlab.com/eread/eread/commit/72441b34c01184c525ea237e6f15cb2d9c5bc3f8))
* reorder Sublime Text configuration page ([b3cc48c](https://gitlab.com/eread/eread/commit/b3cc48c89508f988ef48bc51c02a4f27f57e033b))
* update Development setup page with current set up ([d1c7d26](https://gitlab.com/eread/eread/commit/d1c7d264e58ad1aa3ea182d5fad3d9d05f32f2db))

### 🧱 Project features 🧱

* accept any patch version of Debian packages ([97c39dd](https://gitlab.com/eread/eread/commit/97c39ddd5bef55534f3992b74f63381d5988fc34))
* add comments to Dockerfile ([0536fdf](https://gitlab.com/eread/eread/commit/0536fdfdce3513c530e7cc3d3255b8f31327116b))
* add docker-buildx as a project dependency ([4bdd54b](https://gitlab.com/eread/eread/commit/4bdd54be2c323a107e9d402d6c8649e3ff11c672))
* add more error handing to test-markdown-links shell script ([76246d1](https://gitlab.com/eread/eread/commit/76246d1788db12e31f7cc8ee399c7ea92f481d20))
* add more error handling to test-dockerfile shell script ([781c5e8](https://gitlab.com/eread/eread/commit/781c5e881cfeedc687f5ce3e10c782fc7288ddd9))
* add more error handling to test-html-links shell script ([6598bdf](https://gitlab.com/eread/eread/commit/6598bdf5c10bc416ab185cb3b4957d405e91e6eb))
* add more error handling to test-makefile shell script ([e659fa3](https://gitlab.com/eread/eread/commit/e659fa345b3424fe8360819e51235808cfdecfb8))
* add pipx as Brew dependency ([a0ea689](https://gitlab.com/eread/eread/commit/a0ea6894e83f4aa3239ce9194fbe34e9672c2820))
* add spinner to version table processing ([dec6767](https://gitlab.com/eread/eread/commit/dec6767270cda093f24500639b0bafa0582274a6))
* add tests for GIF generation ([6ceeef2](https://gitlab.com/eread/eread/commit/6ceeef20247901460819f900657acdce1c25048b))
* add URLs to list of tools in versions table ([2e81d2d](https://gitlab.com/eread/eread/commit/2e81d2dee3b21874cb9c449e449719930522f0cd))
* allow menu to be opened in French instead of English ([74aa97a](https://gitlab.com/eread/eread/commit/74aa97ac218a8b01c1d3e6de2f89fada83d52f9a))
* allow update check confirmation to be skipped ([ad5eae1](https://gitlab.com/eread/eread/commit/ad5eae17848aaa13c39845ff2100be4719dca678))
* always ensure Go deps are installed before running GIF tests ([61d078b](https://gitlab.com/eread/eread/commit/61d078bfa2b2127dcf84a1ecc025e91c1fc72956))
* avoid link checking project commit links ([6bd6061](https://gitlab.com/eread/eread/commit/6bd6061b953f48508d3cb2e0efd4cb01e5034e26))
* expand versions list table information ([8b63ddc](https://gitlab.com/eread/eread/commit/8b63ddcbe1362b5b7143e7b0cf0ae35f8b0157ef))
* generate review app when site building shell script changes ([952ce3e](https://gitlab.com/eread/eread/commit/952ce3ec9b14e9ee7cb7f3f88356f5c217ca8bcd))
* get Hugo by using separate build stage ([b240e35](https://gitlab.com/eread/eread/commit/b240e350341551a7d5124b2cfc8fd5a7ba8e462b))
* get Node by using separate build stage ([82fef9a](https://gitlab.com/eread/eread/commit/82fef9a6f39c9e0f3e890748dc42bc2c49525a65))
* get ShellCheck by using a separate build stage ([9be3550](https://gitlab.com/eread/eread/commit/9be35509654eff574990f9c22300545fb2eb280d))
* get Vale by using separate build stage ([263dc28](https://gitlab.com/eread/eread/commit/263dc281b3727485e3c5fb1bc9af1621847b849d))
* go back to mise again ([e144d57](https://gitlab.com/eread/eread/commit/e144d57fee44b43d2c7b299b917cdf3d6316b987))
* highlight new versions of tooling in versions table ([6af983c](https://gitlab.com/eread/eread/commit/6af983cdabc6502d27903925889ca48753d41dcb))
* improve how npm-check-updates tool is run ([6f45a30](https://gitlab.com/eread/eread/commit/6f45a30f5a1574896cd09f4306bc6058d7025307))
* improve version comparison logic ([b72fa0f](https://gitlab.com/eread/eread/commit/b72fa0f631f353254d06b93a9d76442e8c287245))
* improve version determination logic and update dependencies ([8907a65](https://gitlab.com/eread/eread/commit/8907a65de096f0b602d2e0826086558ec102f707))
* incorporate Go code into command-line interface ([41a8f81](https://gitlab.com/eread/eread/commit/41a8f81ec45594e86d7dcb43ae3133b95c46ffec))
* minor Dockerfile improvements ([fb3d9e7](https://gitlab.com/eread/eread/commit/fb3d9e73ef8ba8dd0337fa33078cecbfd8364798))
* move Brew installation logic to shell script ([7293698](https://gitlab.com/eread/eread/commit/7293698d3cc134919247dc894cdc33fab922564a))
* move dependency update logic to Go ([ee45463](https://gitlab.com/eread/eread/commit/ee454636d77275300d8dfdd40cd200f3ed061f44))
* move stylelint configuration to ES module ([c644a4f](https://gitlab.com/eread/eread/commit/c644a4f2a6229ff8e103da3e894b5f50be4af19f))
* move to asdf plugin for lefthook ([d2b7796](https://gitlab.com/eread/eread/commit/d2b7796a6cfa7a23e80e3fcf898e747f5d2035e4))
* move tools URLs into variable ([bc5054e](https://gitlab.com/eread/eread/commit/bc5054e84a519d8edffb969c9e99148f0e6f1440))
* move user prompt into update Go code ([e14da73](https://gitlab.com/eread/eread/commit/e14da7330ddd2e2b8be72eeec278e67e71b2b7f0))
* no longer require Docker when checking for dependency updates ([47006b9](https://gitlab.com/eread/eread/commit/47006b9f4be39e63ad629e2cdd0278640dc01c40))
* output latest tooling versions as a table ([fbdb3bc](https://gitlab.com/eread/eread/commit/fbdb3bc656f90098c58eade1bc47e66fce1fb3b4))
* parameterize Go installation ([fbdad4d](https://gitlab.com/eread/eread/commit/fbdad4de3e0db870dc08b827a7703e8ee975c9e0))
* parameterize Node.js installation ([ff19a86](https://gitlab.com/eread/eread/commit/ff19a86d2c7ea9d81d661b9bcf3e9bc7c75545c3))
* parameterize yamllint version in all places ([5b8ebfa](https://gitlab.com/eread/eread/commit/5b8ebfa8532dda140c68725a57a5ddcd8b1a0a51))
* pin Debian package versions on upstream version only ([61bc8ec](https://gitlab.com/eread/eread/commit/61bc8ecff96cbaa307a281a2c7d7a77c2c7b9ebb))
* refactor how configuration files are read and updated ([63d6f67](https://gitlab.com/eread/eread/commit/63d6f67f5b137b245a3fe552ff6efb6ac5d6a3ae))
* refactor menu appearance ([9837d3e](https://gitlab.com/eread/eread/commit/9837d3e51af58c2cb7176c7f68f701cb72a57237))
* refactor project menu and record demo of project menu as GIF file ([27ce0e7](https://gitlab.com/eread/eread/commit/27ce0e73185a6d0fc4e7fde6d2af537de03efffe))
* remove concurrency limit from link checker ([92cbcbb](https://gitlab.com/eread/eread/commit/92cbcbb46cfb2acf15250042bbcb0ea941095799))
* remove duplication from Dockerfile ([ddfc705](https://gitlab.com/eread/eread/commit/ddfc705cdfcc0abc45dbc285e52d7b3f299ebb9f))
* remove duplication from install and upgrade scripts ([6995941](https://gitlab.com/eread/eread/commit/6995941374746db3f94a6b7ded016b1caa45fe06))
* remove unnecessary variables from update code ([fb4fe9d](https://gitlab.com/eread/eread/commit/fb4fe9da53d2998e38b633acba654db84c8475c6))
* switch to Debian-based Docker image for project ([8a844f0](https://gitlab.com/eread/eread/commit/8a844f07ec430dad27fed9e3e754ab6b371cdf31))
* test Docker files and build under more circumstances ([f37f8df](https://gitlab.com/eread/eread/commit/f37f8dfe1932ef3c0bb54cbcc8e6cc652ed6294e))
* update Go modules when updating project ([1d33621](https://gitlab.com/eread/eread/commit/1d33621defc10b3dec2553c7b9933dedcba632b6))
* update the project update script for new dependencies ([42e6fe1](https://gitlab.com/eread/eread/commit/42e6fe1851565357ecfb8fa6c6ed069d0ff3420b))
* use asdf markdownlint-cli2 plugin instead of Node.js package ([71aecf3](https://gitlab.com/eread/eread/commit/71aecf37a6d13daf241c4c3d5f8c8119c6a4dcc4))
* use caching when building Docker images ([6395d81](https://gitlab.com/eread/eread/commit/6395d814492217d167a3a3947d559845d3dfe090))
* use deployed Docker image to test GIF, not test Docker image ([9ed3f62](https://gitlab.com/eread/eread/commit/9ed3f62901b1c5bf812b274d02836cb8439a7069))
* use GitLab Pages for review apps ([62019e5](https://gitlab.com/eread/eread/commit/62019e5894b6284027d343faa1a7de4f3973fcc4))
* use more Hugo options when building site ([843cba5](https://gitlab.com/eread/eread/commit/843cba5fc8987fe90328b78fb3ae3f10c05fd68e))
* use Puppeteer image to test GIF ([3393356](https://gitlab.com/eread/eread/commit/3393356b2320b778e30110dc5b89055fe2958dea))
* use TARGETARCH instead of BUILDARCH for building Docker image ([e39c3b5](https://gitlab.com/eread/eread/commit/e39c3b5e93ca637250656670e689653e75ea90ab))
* use v2 charmbracelet tooling for menu ([bd43656](https://gitlab.com/eread/eread/commit/bd436564908add574661e05891f831e754848f9e))
* use v2 charmbracelet tooling for update logic ([14717fb](https://gitlab.com/eread/eread/commit/14717fbad37bbd239a5026edd21a88d2cea3e0e0))
* use VHS docker image for VHS binary ([99da7fe](https://gitlab.com/eread/eread/commit/99da7fe38382326bf545edfc3183be726e454f3b))

### 🔧 Project fixes 🔧

* add whitespace in build-docker-image.sh file ([9ae57ee](https://gitlab.com/eread/eread/commit/9ae57ee06d57fe7c710730f6493dc790e0b22dd0))
* add yamllint as build argument to Docker image deployment job ([6d54ce5](https://gitlab.com/eread/eread/commit/6d54ce519f3534ee6d4a802a284b06d215f2bde2))
* disable failing rule introduced by new Stylelint version ([94b5a4d](https://gitlab.com/eread/eread/commit/94b5a4dbefb4b58a4d2196fc5ca4f6c06a254c3f))
* fix bugs and missing tools in pipeline configuration update logic ([ec8d853](https://gitlab.com/eread/eread/commit/ec8d853eb40aaee4c85130638ad36cd760c72c3d))
* fix Docker image publishing job ([5561c6c](https://gitlab.com/eread/eread/commit/5561c6cd7eeac50f509a190ea919ecb546137c10))
* fix incorrect Lefthook configuration ([6a4a5dd](https://gitlab.com/eread/eread/commit/6a4a5dd21887b62a276146ad8ab2e5a961328c2a))
* fix Lychee version lookup ([4b69f8b](https://gitlab.com/eread/eread/commit/4b69f8bf9139c55f968fcdd81f308d458dbf3698))
* fix problem finding corepack when updating Node.js version ([0f50388](https://gitlab.com/eread/eread/commit/0f503885f98728f393dcfc891428c3c91a175dfc))
* fix spelling of information message ([8ec31b3](https://gitlab.com/eread/eread/commit/8ec31b3c1f10c3ee24b3dc79cd82fba49d4a62fb))
* pull correct Docker image for Hugo version check ([1a78637](https://gitlab.com/eread/eread/commit/1a78637a67d327a23867449fcc27698306e6535a))
* refer correctly to ttyd instead of Lychee in version table output ([4fd47b9](https://gitlab.com/eread/eread/commit/4fd47b936519064be729e78e4a7b089383a61ee6))
* update Lefthook configuration for recent Go file changes ([01aae14](https://gitlab.com/eread/eread/commit/01aae14e751aceaa5c790ba604769c4c88df134b))
* update pipeline configuration for recent Go file changes ([8150792](https://gitlab.com/eread/eread/commit/8150792487ee644d25be4fe1513501cf559f2344))
* use correct name of Go code file ([d32dda0](https://gitlab.com/eread/eread/commit/d32dda0973211e3cd8c4b940e715c057ac2cc8a3))
* use SHA for Lychee Docker image because 0.17.0 isn't a valid tag ([32189a0](https://gitlab.com/eread/eread/commit/32189a03c6fef4c73033e11845dc6ff00e0fc665))

## [1.14.0](https://gitlab.com/eread/eread/compare/v1.13.0...v1.14.0) (2024-09-02)

### 🖋️ Content changes 🖋️

* point to Dockerfile again ([41a7d52](https://gitlab.com/eread/eread/commit/41a7d524940cdb0ab59590108f4ae2627e2443ce))
* use link fragments within single page ([28de4d9](https://gitlab.com/eread/eread/commit/28de4d98d8a58c6effbb41ec961b3238421a2825))

### 🧹 Project chores 🧹

* update curl in Dockerfile ([14391f4](https://gitlab.com/eread/eread/commit/14391f48e6e799bb14e6548aecf4b2218fe2a7cb))
* update dependencies ([3b83ac0](https://gitlab.com/eread/eread/commit/3b83ac0df58ebf744a90cfcf200d81701f47d2b4))
* update dependencies ([6d667d7](https://gitlab.com/eread/eread/commit/6d667d731ac3d5b5f64a08c9e4d5866e17427ef6))
* update dependency ([84b5d77](https://gitlab.com/eread/eread/commit/84b5d77baf2bbc516e01eef861bed417c9ba1d56))
* update many dependencies ([83f1aec](https://gitlab.com/eread/eread/commit/83f1aece9efe0b512aa1f67aa5d95606457a5f09))
* update Vale and other dependencies ([bd3b201](https://gitlab.com/eread/eread/commit/bd3b201e461beb8900a7ed95cd019581da0c09b5))
* very minor dependency update ([a74d5a2](https://gitlab.com/eread/eread/commit/a74d5a24b0be010523dfc119d792d45201c4ed47))

### 📔 Documentation updates 📔

* add additional plugins to development setup page ([37c8775](https://gitlab.com/eread/eread/commit/37c8775fc43f401d1da36267410a2d90f58ee26f))
* update contributing documentation with more information ([0fefa94](https://gitlab.com/eread/eread/commit/0fefa949aab0211e90f777dd7882a9daf18240fe))

### 🧱 Project features 🧱

* add --show-error parameter to gum spin calls ([bbbc3f5](https://gitlab.com/eread/eread/commit/bbbc3f5cf07ce745f94a9033b434155b78a06ee6))
* add debug output that a full test run is complete ([2308c06](https://gitlab.com/eread/eread/commit/2308c061e372ad579400463b7431966e93fd928e))
* add Docker image and deployment back to project ([b894ad7](https://gitlab.com/eread/eread/commit/b894ad79f289a3a42202bce19e487ee2a6003df8))
* add error handing to Markdown tests ([4e4f28b](https://gitlab.com/eread/eread/commit/4e4f28b0f711588e2151cdaee29b5232f99742d6))
* add error handling if enabling corepack fails ([b0efb86](https://gitlab.com/eread/eread/commit/b0efb86d66335cfa132ed9b44e42f24748675f45))
* add error handling to build-pdf Make target ([3017445](https://gitlab.com/eread/eread/commit/30174451bcf9e6f1b5b418d6c74ee87b97c42306))
* add error handling to CSS tests ([6290f29](https://gitlab.com/eread/eread/commit/6290f299570fce2106c99332d26e43e45ee90079))
* add error handling to test-markdown.sh script ([0ed1e2b](https://gitlab.com/eread/eread/commit/0ed1e2b976830fcc0272bf284a61c46f8997172f))
* allow site builds to recover if theme files are missing ([7a23f53](https://gitlab.com/eread/eread/commit/7a23f53825c0c88742df39ef76fa7916a75ca379))
* clear project before running tests ([1c7525d](https://gitlab.com/eread/eread/commit/1c7525d8374af8c48866ac18131223df68a39d7c))
* do not cache Docker image builds ([b07063a](https://gitlab.com/eread/eread/commit/b07063af30856fa66b02c49551accd9350c85997))
* further reduce number of Docker images used ([c70b284](https://gitlab.com/eread/eread/commit/c70b284f239301e94ac5445f2d40ae3bd53c5d0b))
* improve error handling of HTML link test ([1246c43](https://gitlab.com/eread/eread/commit/1246c43291b7cb780a64166843077cfc30c66165))
* improve error handling of Markdown link test ([08d4dfd](https://gitlab.com/eread/eread/commit/08d4dfd7e2ef533f62376ada54cc90dc392120d8))
* make Node package update more robust ([7287391](https://gitlab.com/eread/eread/commit/72873917cf1e112eca2210fc83a908caf35c5c80))
* move asdf dependency install logic to shell script ([0c729f3](https://gitlab.com/eread/eread/commit/0c729f390582266b25436d3ac4311396917ee628))
* move Dockerfile test logic to shell script ([29a1ecc](https://gitlab.com/eread/eread/commit/29a1eccc72cbfb4a69dbcc5d581fa52f7b223959))
* move Go test logic to shell script ([0cbc28f](https://gitlab.com/eread/eread/commit/0cbc28f8328aee2ee8aa0eff6523e16ad99645d2))
* move Makefile test logic to shell script ([02b2481](https://gitlab.com/eread/eread/commit/02b2481a3bf99a795127c1cfd907efe45632842c))
* move project clean logic into shell script ([6f2df07](https://gitlab.com/eread/eread/commit/6f2df07add32db07a80da2dd02c91b8fbce52662))
* move shell script test logic to shell script ([e261501](https://gitlab.com/eread/eread/commit/e2615012e49591ab8e1712826d8e7ad1c87002a0))
* move TOML test logic to shell script ([3b223b7](https://gitlab.com/eread/eread/commit/3b223b7939424aa94fda9ec1a644cf74077d81e2))
* no longer use shellcheck Docker image (and update Blowfish theme) ([c8b726b](https://gitlab.com/eread/eread/commit/c8b726b3ce5ed1d786580aa752025327a618e3c8))
* offer to open built PDF ([1ae0d5a](https://gitlab.com/eread/eread/commit/1ae0d5a929f76178804d64a2649d476cc4ecb01b))
* reduce concurrency of tests for additional robustness ([0d9e8e5](https://gitlab.com/eread/eread/commit/0d9e8e52a91218eab8fed0999e67353d55397c33))
* reduce number of Docker images used ([e725735](https://gitlab.com/eread/eread/commit/e7257359bc0dcc07276bf18481252ec5f831b16d))
* simplify test:makefile job ([a6a665e](https://gitlab.com/eread/eread/commit/a6a665ede3ef0c42513bda3d0525ee901815bd0b))
* simplify test:markdown job ([40de1a0](https://gitlab.com/eread/eread/commit/40de1a05e8584e043a825f8dbc4a7de5f68916e1))
* switch to latest Node.js instead of LTS Node.js ([205c9ed](https://gitlab.com/eread/eread/commit/205c9edeee24bcf6eed3c4f5e6b10589cb528758))
* switch YAML linting tool ([fecdab1](https://gitlab.com/eread/eread/commit/fecdab122369281128aa3e7b809c0e46ce055a2d))
* test Docker image builds before push ([85ece51](https://gitlab.com/eread/eread/commit/85ece51444c179fca8074a09fde15cf37b25f2a2))
* use latest Lychee from Alpine distribution ([114e595](https://gitlab.com/eread/eread/commit/114e595f948c39bd95ac0cf062433994d2703d1c))
* use project Docker image ([3496d07](https://gitlab.com/eread/eread/commit/3496d07e930bf0b2049a7014e01f69ebb2f3c9e4))

### 🔧 Project fixes 🔧

* correct debug output text ([8a0357c](https://gitlab.com/eread/eread/commit/8a0357c01a302650f9e5defdfc9efd9738e66a84))
* ensure exit code 1 when failing HTML link tests ([f36f935](https://gitlab.com/eread/eread/commit/f36f93522e999ffc578d45d698c9a7df7327c165))
* fix markdownlint-cli2 violations ([0ce9059](https://gitlab.com/eread/eread/commit/0ce9059f590bc9145730b0dc936b7702705e0bfd))
* install new versions of Node.js before trying to use them ([d1a7089](https://gitlab.com/eread/eread/commit/d1a70899454f2a9f635c0df46f2ba75a2d58cf94))
* remove interactivity from PDF creation script ([fa55897](https://gitlab.com/eread/eread/commit/fa55897942a345d5d133347a6c36ac762c884c65))
* remove unnecessary whitespace from Dockerfile ([080f801](https://gitlab.com/eread/eread/commit/080f80170dd926a8c6af75f37bd126fb0974fb3d))

## [1.13.0](https://gitlab.com/eread/eread/compare/v1.12.0...v1.13.0) (2024-08-15)

### 🖋️ Content changes 🖋️

* allow tables in PDF to break ([113f030](https://gitlab.com/eread/eread/commit/113f0307386f2f9dfc67c11389d0b9890c6fecd8))

### 🧹 Project chores 🧹

* add html-export-pdf-cli as local dependency ([3c6807c](https://gitlab.com/eread/eread/commit/3c6807c41e9aba26986f03ed3ca52d5d373a238d))
* update dependencies ([c0cad7c](https://gitlab.com/eread/eread/commit/c0cad7c6347ba3476db0ecb6c8ab9c5ca631e60f))
* update dependencies ([edc929e](https://gitlab.com/eread/eread/commit/edc929e8759a6c28b67e334e3489699e47f67e28))
* update dependencies on the project ([c44031e](https://gitlab.com/eread/eread/commit/c44031e170a6e4ec9b13bcfb5eab5291cbd36b90))
* update html-export-pdf-cli and remove resolution override ([6b4c906](https://gitlab.com/eread/eread/commit/6b4c90678eb5776df026ad954c9b46e89e20c7da))
* update Hugo and other dependencies ([f148640](https://gitlab.com/eread/eread/commit/f148640fecff00162f4c7c0ea87b2cb166c85735))
* update many of the project dependencies ([152eede](https://gitlab.com/eread/eread/commit/152eede5852b53e4d29b598c7a48c37d3aab016e))
* update project dependencies ([e24bdf3](https://gitlab.com/eread/eread/commit/e24bdf3ece2c03dc5fbb64597d7758ed85ee9bec))
* update very minor Node.js dependency ([b1ae343](https://gitlab.com/eread/eread/commit/b1ae3438cd25c1e338924201b93a3b7c1efaf4b5))

### 📔 Documentation updates 📔

* add details of more plugins ([60549af](https://gitlab.com/eread/eread/commit/60549af3b74512bebe74e5289f43f33d2c306528))
* add information on SublimeLinter-contrib-yamllint configuration ([0102c83](https://gitlab.com/eread/eread/commit/0102c8339ef305ed78aacf7d421ad03c884c74c6))
* move all configuration content to configuration page ([67f3c3e](https://gitlab.com/eread/eread/commit/67f3c3ed8df677a565b0aa71be663c2b7bb5829c))
* update docs for return to asdf ([6e2d08c](https://gitlab.com/eread/eread/commit/6e2d08c2ed74fc7c2eba4ceb2c5149e4e5d728b7))

### 🧱 Project features 🧱

* add gum to test:css job ([fba67f1](https://gitlab.com/eread/eread/commit/fba67f172ea8afcea6fd19a2a372155c1a790107))
* allow update Make target to start Colima ([26180b5](https://gitlab.com/eread/eread/commit/26180b54f5355d819c6f3f9c88d87e4397dee32f))
* enable corepack in case needed ([21b5042](https://gitlab.com/eread/eread/commit/21b50427833ab51436c6d802077b82e093449ca9))
* installation should use asdf as well ([f1d365a](https://gitlab.com/eread/eread/commit/f1d365a73576f1b759c19736a6379b50a8c8671b))
* move test-css Make target logic to shell script ([d1a212d](https://gitlab.com/eread/eread/commit/d1a212dc0c0d34085cc225e7df7d0d537021383a))
* move test-html-links logic to shell script ([fa92361](https://gitlab.com/eread/eread/commit/fa92361cacbeabddb8b99720e6c5399c1f55ca6d))
* move test-markdown logic to shell script ([44fd405](https://gitlab.com/eread/eread/commit/44fd40558b3a318d6e337141e42dbc01348c2ac0))
* move test-markdown-links logic to shell script ([0c4af51](https://gitlab.com/eread/eread/commit/0c4af51ea4d4f196ae4f49b2d4adfeac63ddcc58))
* move update Node.js packages logic to shell script ([e0896b4](https://gitlab.com/eread/eread/commit/e0896b4e17a495a3a569f733a617152e46ed4390))
* return project to asdf for dependencies ([f11747d](https://gitlab.com/eread/eread/commit/f11747d231afe6f67458b28b5411b0c144b1af60))

### 🔧 Project fixes 🔧

* add bash to job that now needs it ([95acd06](https://gitlab.com/eread/eread/commit/95acd06fd32cb5b971959e2e1d8afb76aaa6033b))
* fix copy/paste error in log output ([adbc50d](https://gitlab.com/eread/eread/commit/adbc50d2195d7fef0670f821aa2e60c14b5cd3d3))
* fix Yarn installation for pre-deploy:pdf job ([03c153c](https://gitlab.com/eread/eread/commit/03c153c63335543baef5ab55b6f9e1d01b9fdd49))
* make HTML link checks more stable by reducing concurrency ([7c8a7b0](https://gitlab.com/eread/eread/commit/7c8a7b03e932bf5164cd7b08e24424bcd40ff8be))
* remove unnecessary Lychee parameters ([77d32ec](https://gitlab.com/eread/eread/commit/77d32ecd901202b4145de7a929fc1c5bf99c0591))
* user corepack to install required yarn ([66848ab](https://gitlab.com/eread/eread/commit/66848ab23b40fd48327ec44442d970da66dc9e9b))

## [1.12.0](https://gitlab.com/eread/eread/compare/v1.11.0...v1.12.0) (2024-06-17)

### 🖋️ Content changes 🖋️

* split About me page into multiple pages ([14ea6a9](https://gitlab.com/eread/eread/commit/14ea6a92b4cb5c4a250186dfe2593af12d1def9c))

### 🧹 Project chores 🧹

* again with more dependency updates ([b8a1d94](https://gitlab.com/eread/eread/commit/b8a1d94a11d91995d103052ef3767fe7aece419e))
* even more dependency updates ([20cf8ea](https://gitlab.com/eread/eread/commit/20cf8ea762c9064dc76fd796a389e8faedebbc90))
* further dependency updates ([32769e2](https://gitlab.com/eread/eread/commit/32769e272b34a715c65bd8f9d24014f760e16a96))
* more dependency updates ([82617d9](https://gitlab.com/eread/eread/commit/82617d9be5b3ffa5524ac707964bf6050c334689))
* remove Node.js packages that are only called with yarn dlx ([d3b12fc](https://gitlab.com/eread/eread/commit/d3b12fc5370a8e5fe7211bdd8be3a48daed2b5a7))
* update Hugo and other project dependencies ([1cdcee8](https://gitlab.com/eread/eread/commit/1cdcee8aa1e247b32dc74b3b05c93daa2e1697c6))
* update many project dependencies ([e41b717](https://gitlab.com/eread/eread/commit/e41b7172fb2d9eeafc74e312d5cbe59f6493ba64))
* update Node.js version and some dependencies ([5dc8aa4](https://gitlab.com/eread/eread/commit/5dc8aa4a1c48c5538792be4e5951bb12fe14ac25))
* update project dependencies ([baad793](https://gitlab.com/eread/eread/commit/baad7937b5e164fe8b0edbcd4e81a0ebf691f3a2))
* update project dependencies ([715faab](https://gitlab.com/eread/eread/commit/715faabfcb7914afc87faa62d50ba42e53b4b23a))
* update project dependencies and Docker image versions ([e358a18](https://gitlab.com/eread/eread/commit/e358a185e80bbfc24090be79c8ba3ffdbe0368f4))

### 📔 Documentation updates 📔

* add information about Fmt package ([9359bc1](https://gitlab.com/eread/eread/commit/9359bc1d03eb9dcad308f1e1952ef06d426259f3))
* refactor LSP for Sublime Text plugins section ([359cb67](https://gitlab.com/eread/eread/commit/359cb67652b024c9b7f32a6af468553f20ac6a30))
* refactor Sublime Text packages section ([a2eada0](https://gitlab.com/eread/eread/commit/a2eada00c8d0110a340075815df734d2d1478167))
* refactor SublimeLinter plugins section ([015cb5a](https://gitlab.com/eread/eread/commit/015cb5a51153b394851fda0ee527bc9d2004c345))
* require Homebrew and mise-en-place for project ([dcab646](https://gitlab.com/eread/eread/commit/dcab646b197099b58bd74dae12261714179c7704))
* split development setup page into multiple pages ([91a06ed](https://gitlab.com/eread/eread/commit/91a06ed48a56389397688d5089585ebd15538057))

### 🧱 Project features 🧱

* add bilingual tags ([10f3c03](https://gitlab.com/eread/eread/commit/10f3c032a8b4e0563429691d681f84f0c575bbbf))
* add Colima and Docker as Brew dependencies ([ef1c882](https://gitlab.com/eread/eread/commit/ef1c8823357a6aeda4145b61f98b28e15d70e09d))
* add curl brackets to script calls that use variables ([5becc2a](https://gitlab.com/eread/eread/commit/5becc2a0846d82ca7e8c44c63a541af80db6ff6f))
* add error condition to CSS tests Make target ([222609c](https://gitlab.com/eread/eread/commit/222609cbd93feb2c524fd1b246f550580011676d))
* add error condition to Go tests Make target ([68866e3](https://gitlab.com/eread/eread/commit/68866e30e0cb64743e6474a81f2fcc55927d6864))
* add error handling to create PDF shell script ([25ecef5](https://gitlab.com/eread/eread/commit/25ecef593097a48eb59b01215f0c0187cd088099))
* add french translation of menu options ([6245ad6](https://gitlab.com/eread/eread/commit/6245ad61f7798da7e603e9e312b73678410b96e2))
* add gum to check-docker Make target ([c2b0f5a](https://gitlab.com/eread/eread/commit/c2b0f5aadaca71556b8034cb110be4f0908b2eaa))
* add gum to clean Make target ([ebc2336](https://gitlab.com/eread/eread/commit/ebc2336c6faceefa74e017a5892b337bc5aa1fe6))
* add gum to copy PDF script ([241dd36](https://gitlab.com/eread/eread/commit/241dd364f66927ba6003beed39217b7eaf46e481))
* add gum to create PDF script ([b8766d3](https://gitlab.com/eread/eread/commit/b8766d349660229f9c9a8c1c651413d505e636fd))
* add gum to dependency update script ([0420ba1](https://gitlab.com/eread/eread/commit/0420ba1d698d0c0a81a8898c966b290d556e1de5))
* add gum to Hugo Make targets ([23b6ea8](https://gitlab.com/eread/eread/commit/23b6ea8413966a4ac4d2353ecbb2886c3b72c694))
* add gum to install Make target ([da3de85](https://gitlab.com/eread/eread/commit/da3de8561ca0e3da6c0d7fc2ae628fbb8183b98e))
* add gum to install-lefthook Make target ([c87584f](https://gitlab.com/eread/eread/commit/c87584fbde9dfd4dd9d2a4541bce423684a17e98))
* add gum to some more CI/CD jobs ([71a7f52](https://gitlab.com/eread/eread/commit/71a7f52cb81e05e6333f76095d185602262fd7f9))
* add gum to sync-vale Make target ([fb773ac](https://gitlab.com/eread/eread/commit/fb773ac81cedcd675876596144c06769d67b9945))
* add gum to test-css Make target ([56bda52](https://gitlab.com/eread/eread/commit/56bda522577d089fc4c09e6f2df5bd16fc009830))
* add gum to test-html-links Make target ([a10fe6a](https://gitlab.com/eread/eread/commit/a10fe6a272258c29f2fe4b236640c032b856c4aa))
* add gum to test-makefile Make target ([1606bd8](https://gitlab.com/eread/eread/commit/1606bd8c4e540f712bf8c7609189eb67db90a745))
* add gum to test-markdown Make target ([37dd14d](https://gitlab.com/eread/eread/commit/37dd14d58f70c9658e712e3fe2a14efea4c1fdbc))
* add gum to test-markdown-links Make target ([113a76b](https://gitlab.com/eread/eread/commit/113a76b5a8328e44e94560374843ecc96ee7cb6e))
* add gum to test-shell-scripts Make target ([dfc6383](https://gitlab.com/eread/eread/commit/dfc6383afc27b931d1c6c7d0b33edd0e40f9a254))
* add gum to test-yaml Make target ([350c98b](https://gitlab.com/eread/eread/commit/350c98b54664313d59003f85106febe2e72f5e9d))
* add gum to test:html-links job ([299e2eb](https://gitlab.com/eread/eread/commit/299e2ebeb939666325ccdd3429603fedf3dc2827))
* add gum to update-hugo-mode Make target ([1107a7f](https://gitlab.com/eread/eread/commit/1107a7fe31307065efb0d44920403a99095fed12))
* add gum to update-node-packages ([45a08a2](https://gitlab.com/eread/eread/commit/45a08a27326c4c225c9c872dded54c15384f9897))
* add gum to verify:site-build job ([5fd12f3](https://gitlab.com/eread/eread/commit/5fd12f3f75d5628c1039c458dfb49c9f0d28d4bf))
* add linting for TOML files ([d7f54c7](https://gitlab.com/eread/eread/commit/d7f54c7d0a80545b78d4914b678afa2f5462de8c))
* add mise to install and update Make targets ([27675e2](https://gitlab.com/eread/eread/commit/27675e2b8ee8a6aa627048a78201b8327540d622))
* add spinner to mise install during make update ([a360296](https://gitlab.com/eread/eread/commit/a36029696ca259eccf456e3c8afa6f876f0dd097))
* add spinner to Vale package syncing ([c1c3ac3](https://gitlab.com/eread/eread/commit/c1c3ac3364f4d8554227d0ef2fc6c8ef7e00cc81))
* add tags to English pages ([844d610](https://gitlab.com/eread/eread/commit/844d610242a9ca98ba30c7a9699842f9902c75f9))
* add tags to French pages ([c461881](https://gitlab.com/eread/eread/commit/c46188140267300777a4d5b03c9a75310ee64798))
* add TUI menu to project ([486a392](https://gitlab.com/eread/eread/commit/486a392a60645127cacfb9806e83b706feb6bfe8))
* allow build Make target to run in a CI/CD job ([37d5635](https://gitlab.com/eread/eread/commit/37d56359cd81af7b49c5037df27307dfc575a0b0))
* allow copy PDF script to run in a CI/CD job ([b439e5f](https://gitlab.com/eread/eread/commit/b439e5fd8142ba2f3a529d4fe662fb8834dfbba7))
* dynamically update .gitlab-ci.yml file for more upstream images ([a2a73e0](https://gitlab.com/eread/eread/commit/a2a73e096ac2c106646de58460a099aa0c039cbe))
* enable site search ([6b7c1ac](https://gitlab.com/eread/eread/commit/6b7c1ace0706f36db2e9e33213ecfa1d38bc65cb))
* enhance TUI menu ([56bd930](https://gitlab.com/eread/eread/commit/56bd93096dec1fb7f2e813762b4539f304404f2f))
* handle cases where Docker isn't running ([98bcb48](https://gitlab.com/eread/eread/commit/98bcb487eebba1da12b893720d667ec9f55aa592))
* highlight selected item in TUI ([e6ff460](https://gitlab.com/eread/eread/commit/e6ff460a45882b12b5df7430d7200ec998439fa2))
* make Lefthook run only when needed ([eebf442](https://gitlab.com/eread/eread/commit/eebf4428715f19fdeb3653a8b21728185b19ed3e))
* move to configuration file for Lychee ([dc695ec](https://gitlab.com/eread/eread/commit/dc695eccc5d9c92905a3dfbc9a0d193fc615b596))
* prompt user to update dependencies ([d06d542](https://gitlab.com/eread/eread/commit/d06d54217267d273b63ac03885216d7fb000e3ac))
* provide feedback when update check is cancelled ([95bcc2e](https://gitlab.com/eread/eread/commit/95bcc2eb194a45323a825541c06e02399dfb8067))
* refactor Gum installation on Puppeteer Docker images ([66bdabb](https://gitlab.com/eread/eread/commit/66bdabbb9b68b2ad1ebd24709b04fc166ca2ff0b))
* refactor shell scripts ([21e4eca](https://gitlab.com/eread/eread/commit/21e4ecac4f2c7a657ca06db95f59aa563415fcfd))
* remove sleeps for better performance ([050fa5b](https://gitlab.com/eread/eread/commit/050fa5ba757bdf38f440d5c676d13845a7529ad6))
* remove unneeded package-lock.json file when updating ([873c51c](https://gitlab.com/eread/eread/commit/873c51c0828f7dd69bf9786002c0532745e57c45))
* split up check-docker logic and move to own shell script ([10ae681](https://gitlab.com/eread/eread/commit/10ae681b18a4a42af0509fb8a6fccb2bfb44f4f1))
* switch from Hugo modules to Git submodules for theme ([aaf8ec1](https://gitlab.com/eread/eread/commit/aaf8ec144c8d3fe5de8a40e19fed8cd62409c0a5))
* test HTML links in more cases ([7713929](https://gitlab.com/eread/eread/commit/771392917e60ca9cf2c7f26aa43b4391bdee0295))
* update order of dependencies in .gitlab-ci.yml file ([5665d75](https://gitlab.com/eread/eread/commit/5665d754a40cefe77b10079e7ef819f7731cc0b4))
* use mise to manage Go dependency ([d59bc6a](https://gitlab.com/eread/eread/commit/d59bc6a8032d00e77ebe644ae6f49345bb5a7b6f))
* use mise to manage Hugo dependency ([7bf434a](https://gitlab.com/eread/eread/commit/7bf434a0315bf110eb39dda0e8a7355f18c501e7))
* use mise to manage Node dependency ([519a29c](https://gitlab.com/eread/eread/commit/519a29cb19516e2afbac9d20ab779b79f388a31a))
* use mise to manage ShellCheck dependency ([10123f9](https://gitlab.com/eread/eread/commit/10123f92e4d831ce1d0dae6f07ddc1d77d178858))
* use mise to manage Vale dependency ([9cb16f5](https://gitlab.com/eread/eread/commit/9cb16f57c322ddf3355bd013abeecadaf86510b8))
* use wildcard to pick up shell scripts in support directory ([f048a45](https://gitlab.com/eread/eread/commit/f048a45f4447af3cc8a948b758be92f7b358d042))

### 🔧 Project fixes 🔧

* add submodule variables to pre-deploy:prepare-pdf job ([e491ce4](https://gitlab.com/eread/eread/commit/e491ce4ea2695e914b66a628b017ff1d48384816))
* allow test-html-links Make target to work in CI/CD pipeline ([9fddbb4](https://gitlab.com/eread/eread/commit/9fddbb4ffffff8e8791e87091e584bc21a9042b6))
* allow test:html-links job to work with Git submodules ([ae231fa](https://gitlab.com/eread/eread/commit/ae231fa1ff246b3fb76ba978f132e2a39c0e42f0))
* do not use gum before it is potentially installed ([40120ab](https://gitlab.com/eread/eread/commit/40120ab424ad10e51a4700a271771f198fb57f1b))
* don't print comment in Makefile ([e0ab333](https://gitlab.com/eread/eread/commit/e0ab3339cba1406b2a49c892d6e6360c5528d054))
* downgrade conventional-changelog-conventionalcommits to v7.0.2 ([76629e5](https://gitlab.com/eread/eread/commit/76629e5555d5e75521d4c7287de87c5e0ce8c8e3))

## [1.11.0](https://gitlab.com/eread/eread/compare/v1.10.0...v1.11.0) (2024-05-03)


### 🖋️ Content changes 🖋️

* remove Docker image build from project ([57b30ac](https://gitlab.com/eread/eread/commit/57b30ac1f1941e8e3b66a9042e897f280c4b80ee))


### 📔 Documentation updates 📔

* add information on Markdown Table Formatter customization ([597fec0](https://gitlab.com/eread/eread/commit/597fec0168bb4e38aa5b420668678360bb6a075b))


### 🧱 Project features 🧱

* use upstream image for pages job ([6948a3c](https://gitlab.com/eread/eread/commit/6948a3ca4dc9434129aef8fee52b4cb1ca9e08a8))
* use upstream image for test:css job ([9b6e5cb](https://gitlab.com/eread/eread/commit/9b6e5cb695b51c54a052de642078a14657820704))
* use upstream image for test:html-links job ([b79aa10](https://gitlab.com/eread/eread/commit/b79aa10cd1b5e778cda9e02adf6513700987de5e))
* use upstream image for test:markdown-links job ([b6b9607](https://gitlab.com/eread/eread/commit/b6b9607120584ed5fbcbe2cd3b4bfea7e56a0da7))

## [1.10.0](https://gitlab.com/eread/eread/compare/v1.9.0...v1.10.0) (2024-05-02)


### 🖋️ Content changes 🖋️

* add full stops to all list items ([5d391b3](https://gitlab.com/eread/eread/commit/5d391b3cb6c8407671014bf2cd9196f05f5cd8fb))


### 🧹 Project chores 🧹

* regenerate yarn.lock ([8cd0fec](https://gitlab.com/eread/eread/commit/8cd0fecece382dc2013b484f69e66729922087f1))
* regenerate yarn.lock file ([375f9be](https://gitlab.com/eread/eread/commit/375f9befb5e12bcc7e367ea324810d66d1b41a8a))
* update dependencies for project ([2becfb8](https://gitlab.com/eread/eread/commit/2becfb84fb91dae816724b36d8913bf0548d768f))
* update dependencies for project ([6333db4](https://gitlab.com/eread/eread/commit/6333db407128c53fcdc1ff81e1ec877da5398ef7))
* update project dependencies ([96228eb](https://gitlab.com/eread/eread/commit/96228eb926bc23ceb96e6fc8388e27b481c4e354))
* update project dependencies ([cc0e046](https://gitlab.com/eread/eread/commit/cc0e046b026aee2788dc62fdb74e4852f1d7bfa5))
* update project dependencies ([6ba0eb6](https://gitlab.com/eread/eread/commit/6ba0eb60b2914c2f691faf55869a1dd663d776e5))
* update project dependencies ([5167393](https://gitlab.com/eread/eread/commit/5167393579739cdaab9aa5e0404847820914d061))


### 📔 Documentation updates 📔

* add fast-syntax-highlighting to development setup page ([1882325](https://gitlab.com/eread/eread/commit/18823254e8e6ff15ec3322147a7061a7337c4424))
* translate more of Development setup page into French ([c4a8a60](https://gitlab.com/eread/eread/commit/c4a8a6029f051a27f6a82826b6d2ef0e9b06f0c7))
* update project READMEs to point to respective language pages ([f41a22f](https://gitlab.com/eread/eread/commit/f41a22f1d505ffe4890751aece80d62b093bf0be))


### 🧱 Project features 🧱

* add French grammar rule for lists in French content ([042ad49](https://gitlab.com/eread/eread/commit/042ad4917aa08c798a952ed3e4a4bb0eb277fa41))
* add lychee to project through Brewfile ([8bcc154](https://gitlab.com/eread/eread/commit/8bcc154c5b81b2e2de8c8edd4b9c3d2655bd51cf))
* automatically update to latest stable Yarn ([3b3cdf7](https://gitlab.com/eread/eread/commit/3b3cdf7047f7f8d94b058e2c68b7c32d00f33087))
* install all Brew dependencies into Docker image ([22930db](https://gitlab.com/eread/eread/commit/22930dbb968ca47caaf60b2b80f306aee728e73a))
* install corepack into Docker image ([d5de92a](https://gitlab.com/eread/eread/commit/d5de92a99148d608d9fc359aefe16fc8d6b474f5))
* require corepack for project and not just Node.js ([c0aadd5](https://gitlab.com/eread/eread/commit/c0aadd5dc6dcf1ed8ac0caf000628c6fb59a5541))
* run npm-check-updates as project dependency ([074fe7f](https://gitlab.com/eread/eread/commit/074fe7f1596a8d66593089eac4f6fbf57551b76f))
* support Vale version 3 ([c46a21a](https://gitlab.com/eread/eread/commit/c46a21a058a8564bc30fd0159aa7c65997b402d1))
* use alternative tool for YAML linting ([e18f9b7](https://gitlab.com/eread/eread/commit/e18f9b7a2e86a68b440e8e2aa06f645341e74bce))
* use lychee for link checking ([acf249f](https://gitlab.com/eread/eread/commit/acf249f37302de683b18a73f6b24ebb5f417c339))
* use upstream Docker image for commit messages test ([917f7f0](https://gitlab.com/eread/eread/commit/917f7f0576d3c76b6e9d7c122c55ac7e7d2378f8))
* use upstream Docker image for semantic-release test ([6bb5e2d](https://gitlab.com/eread/eread/commit/6bb5e2d578030d0df4392e82cabd20b61308e9fb))
* use upstream image for build:pdf job ([8bea0f3](https://gitlab.com/eread/eread/commit/8bea0f393af0a0927120a475901fae37e2ba2671))
* use upstream image for pre-deploy:prepare-pdf job ([219f71f](https://gitlab.com/eread/eread/commit/219f71f3e78d8e81e5b468e22e8663ff6fb1e5a2))
* use upstream image for test:makefile job ([ed2724c](https://gitlab.com/eread/eread/commit/ed2724cebae14840fab63e00393fe721439d9ffa))
* use upstream image for test:markdown job ([c5aff73](https://gitlab.com/eread/eread/commit/c5aff73248cb6ed61662d0169ca6207f80c12887))
* use upstream image for test:shell-scripts job ([07e297d](https://gitlab.com/eread/eread/commit/07e297d70d87d3838b29752bad03311be888fffe))
* use upstream image for test:yaml linting ([ca25626](https://gitlab.com/eread/eread/commit/ca2562604d5373ec2dce719b34dbad12d9c6795d))
* use upstream image for verify:site-build job ([29b4638](https://gitlab.com/eread/eread/commit/29b46388745166ba17463f8a891d83d08103a7c5))


### 🔧 Project fixes 🔧

* add git to pre-deploy:release job ([68454e5](https://gitlab.com/eread/eread/commit/68454e541151298fb849d95ef51553eef3b43966))
* avoid unnecessary commands in jobs ([4f60690](https://gitlab.com/eread/eread/commit/4f606908bf835698a4950c05c88070c496b22ebc))
* avoid warning from automatically-generated file ([cb9ade6](https://gitlab.com/eread/eread/commit/cb9ade6ccc0b34fbd6ac92efbc8d52795b589f32))
* do not force link to English about me page from French landing page ([2ec9fbf](https://gitlab.com/eread/eread/commit/2ec9fbf6f48138ed979d52ba5b78e553024ea1f5))
* don't use dlx when library is in project ([863abb7](https://gitlab.com/eread/eread/commit/863abb7bbc2f3740d02a4776854a72569cb338a3))
* fix yamllint configuration ([5bdc376](https://gitlab.com/eread/eread/commit/5bdc376781abfc970e05817578675f8e30fa1e0b))
* remove node_modules to get clean installation ([7c21e9f](https://gitlab.com/eread/eread/commit/7c21e9f448481f4d8f2c8765d9c4896b8b877ed1))
* update jobs to use default Docker image instead ([62001da](https://gitlab.com/eread/eread/commit/62001dab21135e106ed59c6ff2b7cd989aebbc66))
* use pipeline Docker image for more tests ([f14d585](https://gitlab.com/eread/eread/commit/f14d5858ea4b34a109ec5568c6dd1c3d38316dc2))
* use upstream image for pre-deploy:release job ([5e930d3](https://gitlab.com/eread/eread/commit/5e930d3ed26d354a5a063f8d0f3e7aa7d0f2ac28))

## [1.9.0](https://gitlab.com/eread/eread/compare/v1.8.0...v1.9.0) (2024-01-14)


### 🖋️ Content changes 🖋️

* begin translation of About me page ([cb6da6c](https://gitlab.com/eread/eread/commit/cb6da6c9ec27e16628567d4b0870707acf182159))


### 🧹 Project chores 🧹

* migrate rest of project to Yarn 4 ([768109c](https://gitlab.com/eread/eread/commit/768109cb5160ff94ae90ecadb3394cea1cde55c1))
* start tests as soon as possible ([08c9b04](https://gitlab.com/eread/eread/commit/08c9b042f0cacdf6c3238e01f6a468bbe8c590f4))
* use node_modules folder ([dee1cb5](https://gitlab.com/eread/eread/commit/dee1cb5ad83205ce0e2212edf7f593603e7fc188))


### 🧱 Project features 🧱

* migrate PDF creation to Yarn 4 ([336de4b](https://gitlab.com/eread/eread/commit/336de4b2736acbba705c128ad19e32ad2e9192c6))


### 🔧 Project fixes 🔧

* remove duplicate pipeline keyword ([e51076a](https://gitlab.com/eread/eread/commit/e51076a13d3420d8c5bfb7e8b97d56ed162ddaec))

## [1.8.0](https://gitlab.com/eread/eread/compare/v1.7.0...v1.8.0) (2023-12-01)


### 🖋️ Content changes 🖋️

* add page numbers back to PDF output ([a86fbe1](https://gitlab.com/eread/eread/commit/a86fbe12788d413924d625e17d88fcb38bd35d12))


### 🧹 Project chores 🧹

* change PDF-creation tooling ([35ce190](https://gitlab.com/eread/eread/commit/35ce1905a97598a79b894014b8a14da6777a0af5))
* move project to pnpm for package management ([c59f2d6](https://gitlab.com/eread/eread/commit/c59f2d6535d8765a9c952c6b28c350bb5b2fbac8))
* update Blowfish and Lefthook versions ([d502ebc](https://gitlab.com/eread/eread/commit/d502ebcc5425d0247e5946e72131176a2dff9ff8))
* update Blowfish and other dependencies ([8b7283d](https://gitlab.com/eread/eread/commit/8b7283d5df744226150b7f0fe5214a61ebf8433c))
* update NPM-based project dependencies ([9d286c5](https://gitlab.com/eread/eread/commit/9d286c51ac3049bccd7ce2a0e01f712cd4f51207))
* update some project dependencies ([e324da1](https://gitlab.com/eread/eread/commit/e324da1d47d9d6784c167400a65edf9120d912a8))


### 📔 Documentation updates 📔

* add partial French translation of content ([6792048](https://gitlab.com/eread/eread/commit/67920489008b53854cabe93f458262bcb15ec7a9))


### 🧱 Project features 🧱

* fully regenerate package-lock.json file every time ([f4ad1fc](https://gitlab.com/eread/eread/commit/f4ad1fc98014ad28bb872e5456cc41230e9b566f))
* test PDF build with Lefthook ([a342264](https://gitlab.com/eread/eread/commit/a342264aa8cc3d9b60284f7fb97d2711256548c2))


### 🔧 Project fixes 🔧

* do not try to install Node.js on puppeteer image ([f49a4ba](https://gitlab.com/eread/eread/commit/f49a4ba4d564068a82d3461cde4c7972774b18bb))
* ensure jobs that need Node.js and Hugo have prerequisites met ([b182b3d](https://gitlab.com/eread/eread/commit/b182b3d1fd0d73f6b9c53a863307881a3a5eb4a7))
* remove unnecessary configuration ([ad234b4](https://gitlab.com/eread/eread/commit/ad234b4c901ff86fe5caf599dd45ddc4432474a5))

## [1.7.0](https://gitlab.com/eread/eread/compare/v1.6.0...v1.7.0) (2023-10-13)


### 🖋️ Content changes 🖋️

* avoid redirections to GitLab handbook ([3fbe04e](https://gitlab.com/eread/eread/commit/3fbe04e750512a5ffadf31fdfe36ef313ce635c0))


### 🧹 Project chores 🧹

* update many dependencies for project ([da569fa](https://gitlab.com/eread/eread/commit/da569fa7e867016935d8b79acd167ced5f459ed1))
* update project dependencies ([48fee00](https://gitlab.com/eread/eread/commit/48fee004e4472e7b3b243cbc658bd9ca80487e41))
* update puppeteer and other dependencies ([b148ce6](https://gitlab.com/eread/eread/commit/b148ce64a6c473ce7309feb486b8ae8d54e35aa3))
* update some project dependencies ([772dff4](https://gitlab.com/eread/eread/commit/772dff434bbfe64017716e83f411b91f91b72ae8))


### 🧱 Project features 🧱

* reduce Docker image size by not installing software ([de015fa](https://gitlab.com/eread/eread/commit/de015fa2e74186aedbbf6c811a3f53e147b9213b))


### 🔧 Project fixes 🔧

* correct configuration item ([efa6b01](https://gitlab.com/eread/eread/commit/efa6b0109bec8e36916937dae2a7a025e571b0d6))
* don't try to install Node.js on Puppeteer image ([5483960](https://gitlab.com/eread/eread/commit/5483960ba11b63eb045ad1d0e130a78bc3ea6314))
* downgrade Blowfish theme to fix build errors ([186cd7e](https://gitlab.com/eread/eread/commit/186cd7e249e72bdcab65af7c873dcc8def7a1abe))
* include more files that trigger a PDF build ([d78a194](https://gitlab.com/eread/eread/commit/d78a194119a6d815575494f2c71cf207000a6d81))
* install Go when building PDF ([500581d](https://gitlab.com/eread/eread/commit/500581d50fe482c1bb878e57b7a11cab22131706))
* make sure dependent job runs on default branch pipeline ([789bb8f](https://gitlab.com/eread/eread/commit/789bb8fcab52c358dc9c6fcea47f1880a0926556))

## [1.6.0](https://gitlab.com/eread/eread/compare/v1.5.0...v1.6.0) (2023-09-08)


### 🖋️ Content changes 🖋️

* add GitLab Development Kit to specializations ([82a6c6d](https://gitlab.com/eread/eread/commit/82a6c6d0087342994dbe98c70f2e51d46f6e3d2d))


### 🧹 Project chores 🧹

* update Docker and lefthook dependencies ([726beed](https://gitlab.com/eread/eread/commit/726beed0c73dee5f369cb454b46c80e3d621c685))


### 📔 Documentation updates 📔

* add information about SublimeLinter-vale customization ([0203570](https://gitlab.com/eread/eread/commit/02035704af941e9641141c61d0ab5aa3c1f2f72f))


### 🧱 Project features 🧱

* add hadolint as a project dependency ([98fa7c6](https://gitlab.com/eread/eread/commit/98fa7c68785d974ab184af453facd764bf0bf51a))
* add hadolint test ([5332f52](https://gitlab.com/eread/eread/commit/5332f524ccf3726f9a7cedaf9d477b116cea09bd))
* add version output to more Make targets ([6ebc5a8](https://gitlab.com/eread/eread/commit/6ebc5a84fd39bb40c63de6cba72128d92ead3021))
* don't require confirmation to upgrade Node.js dependencies ([689a056](https://gitlab.com/eread/eread/commit/689a056bc9abbb85dea03c533f18b050962f558b))
* make fewer docker run calls to update Dockerfile ([df2cdef](https://gitlab.com/eread/eread/commit/df2cdefa3f16434e1e62a55b9167b2db3192aa0a))
* only deploy new Docker image if certain files change ([431778e](https://gitlab.com/eread/eread/commit/431778e73c6c6956a3a7d751fd6075cb588878c4))
* only deploy new PDF if PDF content or appearance changes ([9f820a3](https://gitlab.com/eread/eread/commit/9f820a3ba6207f29e815430b7b81a5db47047de5))
* reduce size of built Docker image ([fd2a390](https://gitlab.com/eread/eread/commit/fd2a3902333ee81af181202e5980e88f1e3c5b89))
* use Buildx to build Docker images ([2bba2e7](https://gitlab.com/eread/eread/commit/2bba2e7d648e7463a88a2f745d21490077bbd4e6))
* use more variables in .gitlab-ci.yml file ([1da00e0](https://gitlab.com/eread/eread/commit/1da00e0ee9b979d9b146d9899ce75ca10cb10996))
* verify site build only if certain files change ([98291bb](https://gitlab.com/eread/eread/commit/98291bb30b8c2fbc2da81c87c96f503f5e0bbcba))


### 🔧 Project fixes 🔧

* remove dependency between pdf job and pages job ([6992c86](https://gitlab.com/eread/eread/commit/6992c867d285324cb14f454416c1cc0679bf77e8))

## [1.5.0](https://gitlab.com/eread/eread/compare/v1.4.0...v1.5.0) (2023-08-31)


### 🖋️ Content changes 🖋️

* add My interests section ([d69670a](https://gitlab.com/eread/eread/commit/d69670ad619a93d0c799df7e936317b373e55d67))
* add specializations, schedule, and priorities to About me page ([c09868f](https://gitlab.com/eread/eread/commit/c09868f07b3b733cde0c2cfd5ac8a9fd0d6ee326))


### 🧹 Project chores 🧹

* downgrade conventional-changelog-conventionalcommits dependency ([e403a8c](https://gitlab.com/eread/eread/commit/e403a8c4caa2adebb2d2cec551ede17a0dfba524))
* update dependencies ([b4590ea](https://gitlab.com/eread/eread/commit/b4590eaee20fdbdb9eac216c3bbb09238b23a679))
* update markdownlint-cli2 dependency ([9daa68a](https://gitlab.com/eread/eread/commit/9daa68a72d44fd33c6455fe33417792725499e0f))
* update project dependencies ([2cae5c5](https://gitlab.com/eread/eread/commit/2cae5c53781657a34ae22b0706dc6db25198a226))
* update puppeteer Docker image dependency ([00a19a1](https://gitlab.com/eread/eread/commit/00a19a19a50064ef9ccae7023d4d41861cbe43f9))
* update semantic-release dependency ([6498829](https://gitlab.com/eread/eread/commit/6498829e134961d610d3c59062614cb7ea7e9835))
* update version of asdf used in pipelines ([afc4146](https://gitlab.com/eread/eread/commit/afc4146974cd0a45ab6b5e17a4d3c07561b4e25f))


### 🧱 Project features 🧱

* remove semantic-release test ([80f8a0c](https://gitlab.com/eread/eread/commit/80f8a0cbf0b1a30e0f1698a13d1d0aa411ecf5e2))
* restore semantic-release test ([58c10ca](https://gitlab.com/eread/eread/commit/58c10cafe11b6b64ce83c3b3777396e35a0c93e3))
* split post-merge jobs into two stages ([9d2d0ea](https://gitlab.com/eread/eread/commit/9d2d0ea0b734bdc08ea0b4bdcc2a78b32b62dbe7))
* switch back to using latest Docker image by default ([23ee504](https://gitlab.com/eread/eread/commit/23ee50460fa98dfa3958316bfffa32c628514fe0))
* switch from asdf to Brew for dependency management ([87a44bf](https://gitlab.com/eread/eread/commit/87a44bf90cc1c71efb7b9e0149355891eb14ecd9))
* test CSS only if CSS changes ([4b0669f](https://gitlab.com/eread/eread/commit/4b0669f4ccba58f61374606723b7dbf27576df3f))
* test Docker image only when Dockerfile and related files change ([8602326](https://gitlab.com/eread/eread/commit/86023263646ed6cf7a715e5eac8c07df8f21a1de))
* test Makefile only if Makefile changes ([bbaa4fe](https://gitlab.com/eread/eread/commit/bbaa4fe44fddd3fb9d70f38bbd8642dded1c3931))
* test Markdown only if Markdown changes ([1dbd194](https://gitlab.com/eread/eread/commit/1dbd19445cf8be17437be90ea9aa42a328eabc48))
* test PDF only if PDF changes ([90f90c3](https://gitlab.com/eread/eread/commit/90f90c32d02000f54f99f8b65c66828185d6885a))
* test shell scripts only if shell scripts change ([440905b](https://gitlab.com/eread/eread/commit/440905b46b1d8dd8e7664dea6c403b589a714f78))
* test YAML only if YAML changes ([5b8f6eb](https://gitlab.com/eread/eread/commit/5b8f6eb5775b6665f1bdc252040b5fe19a3a4d74))
* use project Docker image to build PDF ([4880e6c](https://gitlab.com/eread/eread/commit/4880e6cd658928d6fbae844c6bc034aa9f895065))


### 🔧 Project fixes 🔧

* add Node.js to test job for Docker image ([856e411](https://gitlab.com/eread/eread/commit/856e4114a05e3ffde92e4221d9e731b740ea5dbd))
* also test a .yaml file when it changes ([86e28cc](https://gitlab.com/eread/eread/commit/86e28cca6ff8e56b4dbe7bf4a76906196af2aa41))
* correct PDF generation ([7aaaf87](https://gitlab.com/eread/eread/commit/7aaaf87fd1d11577337011a42661047265a3baba))
* fix PDF creation in pipelines ([b77086f](https://gitlab.com/eread/eread/commit/b77086f99d0c2fc40b694085454f9694c139bb48))
* have most test use latest published Docker image by default ([59aaa46](https://gitlab.com/eread/eread/commit/59aaa4695b82e81f872823cab1c23832de8dc967))
* remove asdf from PDF deploy job ([e58ea87](https://gitlab.com/eread/eread/commit/e58ea879f31ab8a7ce575606a81831190f45491d))
* stop test job needing itself to complete before starting ([b3f49d8](https://gitlab.com/eread/eread/commit/b3f49d89e6e22d2d61fff3fd84aa2d54d49effe3))
* use Puppeteer image for building PDF ([5478721](https://gitlab.com/eread/eread/commit/5478721bb6b26e22092bfbc6c41b2c2a693e0715))

## [1.4.0](https://gitlab.com/eread/eread/compare/v1.3.0...v1.4.0) (2023-08-18)


### 🖋️ Content changes 🖋️

* add information about how I work and receive feedback ([b8c58c8](https://gitlab.com/eread/eread/commit/b8c58c8fe873bdc42aee2a0fd2545a3fca560590))


### 🧹 Project chores 🧹

* update Blowfish dependency ([9d7fb83](https://gitlab.com/eread/eread/commit/9d7fb83df66ef78739269014e37238d600d74fa2))
* update dependencies ([7fec672](https://gitlab.com/eread/eread/commit/7fec672e1fdf0ebffb5a4ccea25b3a4643d1684a))
* update Hugo and other dependencies ([03a77ba](https://gitlab.com/eread/eread/commit/03a77baf803a7ec74dd3915f48c2936c690f2db2))
* update many dependencies ([a177a39](https://gitlab.com/eread/eread/commit/a177a39435a1d2ef701b073af1791b0c2958d130))
* update Vale dependency ([b90066c](https://gitlab.com/eread/eread/commit/b90066cb4f491e0b5afe0d55d4087565c3dadfed))


### 📔 Documentation updates 📔

* clarify when to use content category ([3b99fa4](https://gitlab.com/eread/eread/commit/3b99fa4ec79e011cdaa7c1df6159008b9d83eba4))


### 🧱 Project features 🧱

* clean module cache before updating modules ([20c68a2](https://gitlab.com/eread/eread/commit/20c68a2b3b0cc4cfcb8179fab86911492c65e87e))
* enable smart table of contents ([0efb361](https://gitlab.com/eread/eread/commit/0efb361cca2b1d94cb6be01f66afd450588b418a))
* make use of more shortcodes ([a67e13b](https://gitlab.com/eread/eread/commit/a67e13b18dcaeb4b7f14f9f3a73d3b72b7a8ed4b))
* replace htmltest with linkinator ([41c3134](https://gitlab.com/eread/eread/commit/41c3134a89b7f15bbb3f7ae3a8f0eb2737b1dbd2))
* replace markdown-link-check with linkinator ([788e818](https://gitlab.com/eread/eread/commit/788e8188f1288b95b395f5d8a206a6a35131edee))
* set Docker default platform to linux/amd64 ([9674a26](https://gitlab.com/eread/eread/commit/9674a26a701f3dc5a55d4fc2cc6833d3fc75ba5e))
* use link icon for link to project repository ([4531385](https://gitlab.com/eread/eread/commit/453138503f9c032008d64264cc8e3c710c44091a))


### 🔧 Project fixes 🔧

* avoid mid-table page breaking in PDF ([5ef2eb8](https://gitlab.com/eread/eread/commit/5ef2eb8ba808dd054ca69637310ec78329599cb6))
* use Make target for testing Markdown links in pipeline ([032b003](https://gitlab.com/eread/eread/commit/032b003d22f54bb59acad4cf2010c94624729488))

## [1.3.0](https://gitlab.com/eread/eread/compare/v1.2.0...v1.3.0) (2023-08-02)


### 🖋️ Content changes 🖋️

* refactor About me page ([dbb6e51](https://gitlab.com/eread/eread/commit/dbb6e51ba7badf4e92fff41408a32d0f65c6011c))


### 🧹 Project chores 🧹

* update project dependencies ([58aba59](https://gitlab.com/eread/eread/commit/58aba5909d13ef1c7e359c1e547a23f490f76315))


### 🧱 Project features 🧱

* manage Vale dependency with asdf ([8d3247e](https://gitlab.com/eread/eread/commit/8d3247e2717dc71531de6c44680d12f56c2e1088))
* update top menu ([7ac2623](https://gitlab.com/eread/eread/commit/7ac2623619f486f894b61489b71e16468071141e))


### 🔧 Project fixes 🔧

* add Markdown formatting to changelog heading ([8fdb804](https://gitlab.com/eread/eread/commit/8fdb8046899d8cd1f0d8a07c215739139270b563))
* clear Vale suggestions ([2285ffa](https://gitlab.com/eread/eread/commit/2285ffac726dc1da37b7f0c9892eaa669858eb0c))

## [1.2.0](https://gitlab.com/eread/eread/compare/v1.1.0...v1.2.0) (2023-07-28)


### 🖋️ Content changes 🖋️

* don't show author information on article pages ([3f923c8](https://gitlab.com/eread/eread/commit/3f923c89e5f9d19d4ec7c4036d4b170854169f48))


### 🧹 Project chores 🧹

* update version of Debian in Dockerfile ([a9c0864](https://gitlab.com/eread/eread/commit/a9c0864fa3a0ff38875f92c7dfdf2a10a53deabf))


### 📔 Documentation updates 📔

* change changelog format for release v1.1.0 ([baeadfa](https://gitlab.com/eread/eread/commit/baeadfa062b48f15b00aee18e7bc1f0a02078ec5))
* clarify content change category ([457c6e8](https://gitlab.com/eread/eread/commit/457c6e81dcfca6b0eb0608bb84db1afa9c14ef6f))


### 🧱 Project features 🧱

* add title to CHANGELOG.md ([e53594e](https://gitlab.com/eread/eread/commit/e53594e5458f170948d77153f0b12fae48c59910))
* configure different changelog headings ([72e522a](https://gitlab.com/eread/eread/commit/72e522a1fe850ea6dc722fc38618e704a348ad3c))
* configure release notes generator ([b53808c](https://gitlab.com/eread/eread/commit/b53808c732c9ecbc373057feeb39aff0d4aed14a))
* remove label from released PDF asset ([88941cc](https://gitlab.com/eread/eread/commit/88941cc61a21de3507e04c45e57d8f53e411ae04))

## 1.1.0 (2023-07-27)

Includes all commits up to tag [`v1.1.0`](https://gitlab.com/eread/eread/-/commits/v1.1.0?ref_type=tags).

## 1.0.0 (2023-07-26)

Includes all commits up to tag [`v1.0.0`](https://gitlab.com/eread/eread/-/commits/v1.0.0?ref_type=tags).
