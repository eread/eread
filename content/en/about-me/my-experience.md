---
title: 'My experience'
showTableOfContents: true
summary: 'My technical writing experience and experience with tooling.'
tags:
  - 'me / moi'
---

<!-- vale french.french-lists-colons = NO -->
I combine general technical writing experience with experience in specific tools
and technologies.

## Technical writing experience

My technical writing experience includes:

- Content writing and editing.
- Information architecture design and implementation.
- Maintaining documentation projects, including build and publishing pipelines.

For examples, see my
[GitLab contributions]({{< ref "my-work-at-gitlab.md#gitlab-contributions" >}}).

## Tools experience

I have experience with tools for technical writing and publishing. The [`eread` project](https://gitlab.com/eread/eread)
provides some examples.

{{< tools-experience >}}

For examples, see my
[GitLab contributions]({{< ref "my-work-at-gitlab.md#gitlab-contributions" >}}).
