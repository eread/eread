---
title: 'My work at GitLab'
showTableOfContents: true
summary: 'How I like to work, some of the things I value, and some of the things I do at GitLab.'
tags:
  - 'me / moi'
---

<!-- vale french.french-lists-colons = NO -->
Some information about how I like to work, some of the things I value, and some
of the things I do at GitLab.

## Specializations

As well as my technical writing work at GitLab, I specialize in:

- [DocOps](https://www.writethedocs.org/guide/doc-ops/) for the [GitLab product documentation](https://docs.gitlab.com),
  including linting and testing.
- Command line user interfaces.
- [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit).

## Schedule and priorities

I work a standard day on Monday to Friday in the
[AEST timezone](https://www.timeanddate.com/time/zones/aest). I sometimes work
outside of those hours to accommodate meetings with colleagues.

I work on a [maker's schedule](http://paulgraham.com/makersschedule.html), not a
manager's schedule.

On any given day, I work to these priorities:

1. Reviewing other's work to unblock them.
1. Continuing my own work until I become blocked and need someone else to
   unblock me.
1. Starting new work with an end-of-milestone due date.
1. Starting new work with an end-of-quarter due date.
1. Starting new work without a due date.

## Preferred working style

I believe I work best when I can work:

- Asynchronously and in written format. This allows me time to think deeply and
  locate resources to help make decisions.
- Toward well-defined goals, but have freedom to choose the path to get to those
  goals.
- With alignment to my [favorite GitLab values](#favorite-gitlab-values).

I like to focus on tasks and throughput, but see colleagues as more than their output.

## Receiving feedback

I like to receive feedback:

- Often, so I can keep track of how my work within GitLab and with colleagues.
- In written form and asynchronously, which aligns with my preferred working style.

## Favorite GitLab values

My favorite [GitLab values](https://about.gitlab.com/handbook/values/).

{{< favorite-gitlab-values >}}

## Favorite GitLab communication principle

I believe we should [start with a merge request](https://about.gitlab.com/handbook/communication/#start-with-a-merge-request),
because:

- Merge requests encourage folks to aim at solutions and refine the problems to solve.
- Discussions on solutions become asynchronous and considered.
- The impact of possible solutions become easier to visualize.

Advantages of starting with merge requests instead of issues:

- More focussed discussions.
- Merge requests require folks to solve problems iteratively, because it becomes
  clear when a solution becomes too big.

## GitLab contributions

I often contribute to these GitLab projects.

{{< gitlab-contributions >}}

### Video content

I've contributed the following video to [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A).

<!-- vale Vale.Spelling = NO -->

{{< youtubeLite id=aMFcdGtMCaY >}}
