---
title: 'Development setup'
showTableOfContents: true
summary: 'My development setup.'
tags:
  - 'dev / dév'
---
<!-- vale french.french-lists-colons = NO -->
<!-- markdownlint-configure-file { "line-length": { "line_length": 120 } } -->
My development setup includes:

- macOS with:
  - [Hidden Bar](https://github.com/dwarvesf/hidden).
  - [iStat Menus](https://bjango.com/mac/istatmenus/).
- [Terminal](https://en.wikipedia.org/wiki/Terminal_(macOS)), [Zsh](https://www.zsh.org), and
  [Oh My Zsh](https://ohmyz.sh) with:
  - These plugins:
    - [`autoupdate`](https://github.com/tamcore/autoupdate-oh-my-zsh-plugins).
    - [`fast-syntax-highlighting`](https://github.com/zdharma-continuum/fast-syntax-highlighting).
    - [`git`](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/git).
    - [`mise`](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/mise).
    - [`zsh-autosuggestions`](https://github.com/zsh-users/zsh-autosuggestions).
    - [`zsh-completions`](https://github.com/zsh-users/zsh-completions).
  - This theme: [Powerlevel10k](https://github.com/romkatv/powerlevel10k).
- [Sublime Text](https://www.sublimetext.com) and [Sublime Merge](https://www.sublimemerge.com).

I manage software packages and runtime environments with:

- Homebrew: <https://brew.sh>.
- `mise`: <https://mise.jdx.dev> (if available).

## Sublime Text setup

For detailed information on how I set up Sublime Text, see the following sections.
