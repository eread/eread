---
title: 'Sur moi'
showTableOfContents: true
summary: 'Un rédacteur technique expérimenté.'
tags:
  - 'me / moi'
---

<!-- vale Vale.Spelling = NO -->

Un rédacteur **technique** expérimenté.

- Mon profil GitLab : [`eread`](https://gitlab.com/eread).
- Mon profil d'équipe : [Evan Read](https://about.gitlab.com/company/team/#eread).
- Ma fiche de poste : [Senior Technical Writer](https://handbook.gitlab.com/job-families/product/technical-writer/#senior-technical-writer)
  (en anglais).

## Mes intérêts

En dehors du travail, mes centres d'intérêt sont :

- L’histoire.
- Les langues (le français en particulier).
- Le football australien.

Lire plus d'informations [sur moi]({{< relref path="about-me.md" lang="en" >}})
(en anglais).
