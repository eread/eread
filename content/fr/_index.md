---
title: "LISEZ-MOI d'Evan Read"
---

<!-- vale Vale.Spelling = NO -->

{{< typeit
  speed=50
  lifeLike=true
  waitUntilVisible=true
>}}
Un rédacteur <strong>technique</strong> expérimenté.
{{< /typeit >}}

Lire des informations [sur moi]({{< relref path="about-me.md" >}}) et
[sur mon environnement de développement]({{< relref path="about-me.md" >}}).
