---
title: 'Environnement de développement'
showTableOfContents: true
summary: 'Mon environnement de développement.'
tags:
  - 'dev / dév'
---

<!-- vale Vale.Spelling = NO -->
<!-- markdownlint-configure-file { "line-length": { "line_length": 120 } } -->

Mon environnement de développement comprend :

- macOS avec :
  - [Hidden Bar](https://github.com/dwarvesf/hidden).
  - [iStat Menus](https://bjango.com/mac/istatmenus/).
- [Terminal](https://fr.wikipedia.org/wiki/Terminal_(macOS)), [Zsh](https://www.zsh.org) et
  [Oh My Zsh](https://ohmyz.sh) avec :
  - Ces plugins :
    - [`autoupdate`](https://github.com/tamcore/autoupdate-oh-my-zsh-plugins).
    - [`fast-syntax-highlighting`](https://github.com/zdharma-continuum/fast-syntax-highlighting).
    - [`git`](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/git).
    - [`mise`](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/mise).
    - [`zsh-autosuggestions`](https://github.com/zsh-users/zsh-autosuggestions).
  - Ce thème : [Powerlevel10k](https://github.com/romkatv/powerlevel10k).
- [Sublime Text](https://www.sublimetext.com) et [Sublime Merge](https://www.sublimemerge.com).

Je gère des paquets de logiciels et des environnements d'exécution avec :

- Homebrew : <https://brew.sh>.
- `mise` : <https://mise.jdx.dev> (si disponible).

Lire plus d'informations [sur mon environnement de développement]({{< relref path="about-me.md" lang="en" >}}) (en anglais).
