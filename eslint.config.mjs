import eslintPluginToml from 'eslint-plugin-toml';
export default [
  ...eslintPluginToml.configs['flat/standard'],
];
